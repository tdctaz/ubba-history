using System.Collections.Generic;
using System.IO;
using System.Linq;
using BloodBowl.Core;
using BloodBowl.DomainModel.Roster;
using Microsoft.Office.Interop.Excel;

namespace BloodBowl.Roster
{
	public class RosterSerializer : ExcelSerializerBase
	{
		public IRoster ReadRoster(string filepath)
		{
			if (File.Exists(filepath) == false)
			{
				throw new FileNotFoundException("The match report was not found.", filepath);
			}

			var workbook = Workbooks.Open(filepath);
			try
			{
				Worksheet sheet = workbook.Sheets[1];
				return ReadRoster(sheet);
			}
			finally
			{
				workbook.Close(SaveChanges: false);
				ReleaseComObject(workbook);
			}
		}

		private Roster ReadRoster(Worksheet sheet)
		{
			var teamDetails = ReadPlayerTypeDetails(sheet);

			var roster = new Roster
			{
				ReRolls = GetAsInt(sheet, "T20"),
				FanFactor = GetAsInt(sheet, "T21"),
				AssCoaches = GetAsInt(sheet, "T22"),
				Cheerleaders = GetAsInt(sheet, "T23"),
				Apothecary = GetAsInt(sheet, "T24"),
				TeamName = GetAsString(sheet, "I20"),
				TeamTag = GetAsString(sheet, "J20"),
				Race = GetAsString(sheet, "I21"),
				Coach = GetAsString(sheet, "I22"),
				Treasury = GetAsInt(sheet, "I24"),
				NotesLeft = GetAsString(sheet, "C19"),
				NotesRight = GetAsString(sheet, "AB19"),
				Players = 3.RangeTo(18).Select(x => ReadRosterPlayers(sheet, x.ToString())).ToArray(),
				Details = new TeamDetails
				{
					ReRollCost = GetAsInt(sheet, "V20") / 1000,
					PlayerDetails = 2.RangeTo(15).Select(x => ReadPlayerDetails(sheet, teamDetails, x.ToString())).Where(x => x != null && string.IsNullOrEmpty(x.TypeName) == false).ToArray(),
				},
			};
			return roster;
		}

		private PlayerDetails ReadPlayerDetails(Worksheet sheet, Dictionary<string, PlayerTypeDetails> teamDetails, string row)
		{
			string typename = GetAsString(sheet, "BT" + row);
			if (string.IsNullOrEmpty(typename))
			{
				return null;
			}
			var detail = teamDetails[typename.ToLower()];
			var playerDetails = new PlayerDetails
			{
				TypeName = detail.TypeName,
				Cost = detail.Cost,
				ShortTypeName = detail.TypeName.SubstringUntil('(').Trim(),
				Journeyman = detail.TypeName.ToLower().Contains("journey"),
			};
			return playerDetails;
		}

		private Player ReadRosterPlayers(Worksheet sheet, string row)
		{
			var player = new Player
			{
				Number = GetAsInt(sheet, "B" + row),
				Name = GetAsString(sheet, "C" + row),
				Type = GetAsString(sheet, "D" + row),
				MissNextMatch = GetAsBool(sheet, "L" + row),
				NigglingInjury = GetAsInt(sheet, "M" + row),
				InjuriesMovement = GetAsInt(sheet, "N" + row),
				InjuriesStrength = GetAsInt(sheet, "O" + row),
				InjuriesAgility = GetAsInt(sheet, "P" + row),
				InjuriesArmor = GetAsInt(sheet, "Q" + row),
				ActInterceptions = GetAsInt(sheet, "R" + row),
				ActCompletions = GetAsInt(sheet, "S" + row),
				ActTouchdowns = GetAsInt(sheet, "T" + row),
				ActCasaulties = GetAsInt(sheet, "U" + row),
				ActKills = GetAsInt(sheet, "V" + row),
				MVPs = GetAsInt(sheet, "W" + row),
				BonusValue = GetAsInt(sheet, "Z" + row),
				BonusSSP = GetAsInt(sheet, "AA" + row),
				Upgrade1 = GetAsString(sheet, "AB" + row),
				Upgrade2 = GetAsString(sheet, "AC" + row),
				Upgrade3 = GetAsString(sheet, "AD" + row),
				Upgrade4 = GetAsString(sheet, "AE" + row),
				Upgrade5 = GetAsString(sheet, "AF" + row),
				Upgrade6 = GetAsString(sheet, "AG" + row),
				BonusUpgrades = GetAsString(sheet, "AH" + row),
			};
			return player;
		}

		public void WriteRoster(string filepath, IRoster roster)
		{
			CreateRoster(filepath);
			var workbook = Workbooks.Open(filepath);
			try
			{
				Worksheet sheet = workbook.Sheets[1];
				WriteRoster(sheet, roster);
			}
			finally
			{
				workbook.Save();
				workbook.Close(SaveChanges: false);
				ReleaseComObject(workbook);
			}
		}

		private void WriteRoster(Worksheet sheet, IRoster roster)
		{
			SetValue(sheet, "T20", roster.ReRolls.ToString());
			SetValue(sheet, "T21", roster.FanFactor.ToString());
			SetValue(sheet, "T22", roster.AssCoaches.ToString());
			SetValue(sheet, "T23", roster.Cheerleaders.ToString());
			SetValue(sheet, "T24", roster.Apothecary.ToString());
			SetValue(sheet, "I20", roster.TeamName);
			SetValue(sheet, "J20", roster.TeamTag);
			SetValue(sheet, "I21", roster.Race);
			SetValue(sheet, "I22", roster.Coach);
			SetValue(sheet, "I24", roster.Treasury.ToString());
			SetValue(sheet, "C19", roster.NotesLeft);
			SetValue(sheet, "AB19", roster.NotesRight);
			for (int i = 0; i < 16; i++)
			{
				WritePlayer(sheet, roster.Players[i], (i+3).ToString());
			}
		}

		private void WritePlayer(Worksheet sheet, IPlayer player, string row)
		{
			SetValue(sheet, "C" + row, player.Name);
			SetValue(sheet, "D" + row, player.Type);
			SetValue(sheet, "L" + row, player.MissNextMatch ? "x" : null);
			SetValue(sheet, "M" + row, player.NigglingInjury.ToString(), "0");
			SetValue(sheet, "N" + row, player.InjuriesMovement.ToString(), "0");
			SetValue(sheet, "O" + row, player.InjuriesStrength.ToString(), "0");
			SetValue(sheet, "P" + row, player.InjuriesAgility.ToString(), "0");
			SetValue(sheet, "Q" + row, player.InjuriesArmor.ToString(), "0");
			SetValue(sheet, "R" + row, player.ActInterceptions.ToString(), "0");
			SetValue(sheet, "S" + row, player.ActCompletions.ToString(), "0");
			SetValue(sheet, "T" + row, player.ActTouchdowns.ToString(), "0");
			SetValue(sheet, "U" + row, player.ActCasaulties.ToString(), "0");
			SetValue(sheet, "V" + row, player.ActKills.ToString(), "0");
			SetValue(sheet, "W" + row, player.MVPs.ToString(), "0");
			SetValue(sheet, "Z" + row, player.BonusValue.ToString(), "0");
			SetValue(sheet, "AA" + row, player.BonusSSP.ToString(), "0");
			SetValue(sheet, "AB" + row, player.Upgrade1);
			SetValue(sheet, "AC" + row, player.Upgrade2);
			SetValue(sheet, "AD" + row, player.Upgrade3);
			SetValue(sheet, "AE" + row, player.Upgrade4);
			SetValue(sheet, "AF" + row, player.Upgrade5);
			SetValue(sheet, "AG" + row, player.Upgrade6);
			SetValue(sheet, "AH" + row, player.BonusUpgrades);
		}

		private void CreateRoster(string filepath)
		{
			using (Stream embeddedResource = GetType().Assembly.GetManifestResourceStream("BloodBowl.Roster.embedded.outputroster.xlsx"))
			{
				using (var fileStream = File.Create(filepath))
				{
					embeddedResource.CopyTo(fileStream);
				}
			}
		}

		private Dictionary<string, PlayerTypeDetails> ReadPlayerTypeDetails(Worksheet sheet)
		{
			return 3.RangeTo(190).Select(x => ReadPlayerTypeDetail(sheet, x.ToString())).ToDictionary(x => x.TypeName.ToLower());
		}

		private PlayerTypeDetails ReadPlayerTypeDetail(Worksheet sheet, string row)
		{
			var playerDetails = new PlayerTypeDetails
			{
				TypeName = GetAsString(sheet, "AX" + row),
				MA = GetAsString(sheet, "AY" + row),
				ST = GetAsString(sheet, "AZ" + row),
				AG = GetAsString(sheet, "BA" + row),
				AV = GetAsString(sheet, "BB" + row),
				Skills = GetAsString(sheet, "BC" + row),
				Cost = GetAsInt(sheet, "BD" + row) / 1000,
				GeneralValue = GetAsString(sheet, "BF" + row),
				AgilityValue = GetAsString(sheet, "BG" + row),
				PassingValue = GetAsString(sheet, "BH" + row),
				StrengthValue = GetAsString(sheet, "BI" + row),
				MutationValue = GetAsString(sheet, "BJ" + row),
				MaxQuantity = GetAsInt(sheet, "BK" + row),
			};
			return playerDetails;
		}

		private class PlayerTypeDetails
		{
			public string TypeName;
			public string MA;
			public string ST;
			public string AG;
			public string AV;
			public string Skills;
			public int Cost;
			public string GeneralValue;
			public string AgilityValue;
			public string PassingValue;
			public string StrengthValue;
			public string MutationValue;
			public int MaxQuantity;
		}
	}
}
