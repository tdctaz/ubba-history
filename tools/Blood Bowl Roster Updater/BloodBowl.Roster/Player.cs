using System;
using BloodBowl.DomainModel.Roster;

namespace BloodBowl.Roster
{
	public class Player : IPlayer, IEquatable<Player>
	{
		public Player()
		{
			Name = string.Empty;
			Type = string.Empty;
			Upgrade1 = string.Empty;
			Upgrade2 = string.Empty;
			Upgrade3 = string.Empty;
			Upgrade4 = string.Empty;
			Upgrade5 = string.Empty;
			Upgrade6 = string.Empty;
			BonusUpgrades = string.Empty;
		}

		public int Number
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Type
		{
			get;
			set;
		}

		public bool MissNextMatch
		{
			get;
			set;
		}

		public int NigglingInjury
		{
			get;
			set;
		}

		public int InjuriesMovement
		{
			get;
			set;
		}

		public int InjuriesStrength
		{
			get;
			set;
		}

		public int InjuriesAgility
		{
			get;
			set;
		}

		public int InjuriesArmor
		{
			get;
			set;
		}

		public int ActInterceptions
		{
			get;
			set;
		}

		public int ActCompletions
		{
			get;
			set;
		}

		public int ActTouchdowns
		{
			get;
			set;
		}

		public int ActCasaulties
		{
			get;
			set;
		}

		public int ActKills
		{
			get;
			set;
		}

		public int MVPs
		{
			get;
			set;
		}

		public string Upgrade1
		{
			get;
			set;
		}

		public string Upgrade2
		{
			get;
			set;
		}

		public string Upgrade3
		{
			get;
			set;
		}

		public string Upgrade4
		{
			get;
			set;
		}

		public string Upgrade5
		{
			get;
			set;
		}

		public string Upgrade6
		{
			get;
			set;
		}

		public string BonusUpgrades
		{
			get;
			set;
		}

		public int BonusSSP
		{
			get;
			set;
		}

		public int BonusValue
		{
			get;
			set;
		}

		public bool IsEmpty
		{
			get
			{
				return string.IsNullOrEmpty(Type);
			}
		}

		public bool IsJourneyPlayer
		{
			get
			{
				return Type.ToLower().Contains("journey");
			}
		}

		public override string ToString()
		{
			if (IsEmpty)
			{
				return string.Format("[#{0} <empty slot>]", Number);
			}
			if (IsJourneyPlayer)
			{
				return string.Format("[#{0} <unnamed> ({1})]", Number, Type);
			}
			return string.Format("[#{0} {1} ({2})]", Number, Name, Type);
		}

		public bool Equals(Player other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}
			return other.Number == Number && Equals(other.Name, Name) && Equals(other.Type, Type) && other.MissNextMatch.Equals(MissNextMatch) && other.NigglingInjury == NigglingInjury && other.InjuriesMovement == InjuriesMovement && other.InjuriesStrength == InjuriesStrength && other.InjuriesAgility == InjuriesAgility && other.InjuriesArmor == InjuriesArmor && other.ActInterceptions == ActInterceptions && other.ActCompletions == ActCompletions && other.ActTouchdowns == ActTouchdowns && other.ActCasaulties == ActCasaulties && other.ActKills == ActKills && other.MVPs == MVPs && Equals(other.Upgrade1, Upgrade1) && Equals(other.Upgrade2, Upgrade2) && Equals(other.Upgrade3, Upgrade3) && Equals(other.Upgrade4, Upgrade4) && Equals(other.Upgrade5, Upgrade5) && Equals(other.Upgrade6, Upgrade6) && Equals(other.BonusUpgrades, BonusUpgrades) && other.BonusSSP == BonusSSP && other.BonusValue == BonusValue;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof (Player))
			{
				return false;
			}
			return Equals((Player)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int result = Number;
				result = (result*397) ^ (Name != null ? Name.GetHashCode() : 0);
				result = (result*397) ^ (Type != null ? Type.GetHashCode() : 0);
				result = (result*397) ^ MissNextMatch.GetHashCode();
				result = (result*397) ^ NigglingInjury;
				result = (result*397) ^ InjuriesMovement;
				result = (result*397) ^ InjuriesStrength;
				result = (result*397) ^ InjuriesAgility;
				result = (result*397) ^ InjuriesArmor;
				result = (result*397) ^ ActInterceptions;
				result = (result*397) ^ ActCompletions;
				result = (result*397) ^ ActTouchdowns;
				result = (result*397) ^ ActCasaulties;
				result = (result*397) ^ ActKills;
				result = (result*397) ^ MVPs;
				result = (result*397) ^ (Upgrade1 != null ? Upgrade1.GetHashCode() : 0);
				result = (result*397) ^ (Upgrade2 != null ? Upgrade2.GetHashCode() : 0);
				result = (result*397) ^ (Upgrade3 != null ? Upgrade3.GetHashCode() : 0);
				result = (result*397) ^ (Upgrade4 != null ? Upgrade4.GetHashCode() : 0);
				result = (result*397) ^ (Upgrade5 != null ? Upgrade5.GetHashCode() : 0);
				result = (result*397) ^ (Upgrade6 != null ? Upgrade6.GetHashCode() : 0);
				result = (result*397) ^ (BonusUpgrades != null ? BonusUpgrades.GetHashCode() : 0);
				result = (result*397) ^ BonusSSP;
				result = (result*397) ^ BonusValue;
				return result;
			}
		}
	}
}
