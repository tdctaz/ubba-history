using System.Collections.Generic;
using System.Linq;
using BloodBowl.DomainModel.Roster;

namespace BloodBowl.Roster
{
	public class Roster : IRoster
	{
		public Roster()
		{
			Players = new IPlayer[16];
		}

		public string TeamName
		{
			get;
			set;
		}

		public string TeamTag
		{
			get;
			set;
		}

		public string Race
		{
			get;
			set;
		}

		public string Coach
		{
			get;
			set;
		}

		public string NotesLeft
		{
			get;
			set;
		}

		public string NotesRight
		{
			get;
			set;
		}

		public int ReRolls
		{
			get;
			set;
		}

		public int FanFactor
		{
			get;
			set;
		}

		public int AssCoaches
		{
			get;
			set;
		}

		public int Cheerleaders
		{
			get;
			set;
		}

		public int Apothecary
		{
			get;
			set;
		}

		public IPlayer[] Players
		{
			get;
			set;
		}

		public IEnumerable<IMatchHistory> MatchHistory
		{
			get;
			set;
		}

		public ITeamDetails Details
		{
			get;
			set;
		}

		public int Treasury
		{
			get;
			set;
		}

		public int CountActivePlayers()
		{
			return Players.Count(x => x.IsEmpty == false && x.MissNextMatch == false);
		}
	}
}
