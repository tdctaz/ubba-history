using System.Collections.Generic;
using BloodBowl.DomainModel.Roster;

namespace BloodBowl.Roster
{
	public class TeamDetails : ITeamDetails
	{
		public int ReRollCost
		{
			get;
			set;
		}

		public IEnumerable<IPlayerDetails> PlayerDetails
		{
			get;
			set;
		}
	}
}
