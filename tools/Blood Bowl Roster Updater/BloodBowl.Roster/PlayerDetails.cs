using BloodBowl.DomainModel.Roster;

namespace BloodBowl.Roster
{
	public class PlayerDetails : IPlayerDetails
	{
		public string TypeName
		{
			get;
			set;
		}

		public string ShortTypeName
		{
			get;
			set;
		}

		public int Cost
		{
			get;
			set;
		}

		public bool Journeyman
		{
			get;
			set;
		}
	}
}
