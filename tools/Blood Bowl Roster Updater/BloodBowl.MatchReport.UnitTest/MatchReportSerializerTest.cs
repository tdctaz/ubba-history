using System;
using System.IO;
using System.Linq;
using BloodBowl.DomainModel.MatchReport;
using Xunit;

namespace BloodBowl.MatchReport.UnitTest
{
	public class MatchReportSerializerTest : IDisposable
	{
		private readonly MatchReportSerializer _matchReportSerializer;

		public MatchReportSerializerTest()
		{
			_matchReportSerializer = new MatchReportSerializer();
		}

		public void Dispose()
		{
			_matchReportSerializer.Dispose();
		}

		[Fact]
		public void WhenOperingMatchReportFile_ThenMatchReportFileIsOpened()
		{
			var matchReport = _matchReportSerializer.ReadMatchReport(Path.Combine(TestFilesPath, "blankmatchreport.xlsx"));

			Assert.NotNull(matchReport);
		}

		[Fact]
		public void WhenOpetingEmptyMatchReport_ThenMatchReportWithEmptyValuesIsReturned()
		{
			var matchReport = _matchReportSerializer.ReadMatchReport(Path.Combine(TestFilesPath, "blankmatchreport.xlsx"));

			Assert.Equal(string.Empty, matchReport.MatchNumber);
			Assert.Equal(string.Empty, matchReport.MatchType);
			Assert.Equal(string.Empty, matchReport.Report);
			AssertTeamEmpty(matchReport.HomeTeam);
			AssertTeamEmpty(matchReport.AwayTeam);
			Assert.NotEmpty(matchReport.MatchActions);
			foreach (var matchAction in matchReport.MatchActions)
			{
				Assert.Equal(string.Empty, matchAction.Action);
				Assert.Equal(string.Empty, matchAction.Half);
				Assert.Equal(0, matchAction.SourcePlayer);
				Assert.Equal(0, matchAction.TargetPlayer);
				Assert.Equal(string.Empty, matchAction.TeamTag);
				Assert.Equal(0, matchAction.Turn);
			}
		}

		private static void AssertTeamEmpty(ITeamReport team)
		{
			Assert.Equal(0, team.FAME);
			Assert.Equal(0, team.FansRoll);
			Assert.Equal(0, team.Gate);
			Assert.Equal(0, team.Inducement);
			Assert.Empty(team.Inducements);
			Assert.Equal(0, team.MVP);
			Assert.Equal(0, team.PettyCash);
			Assert.Equal(0, team.Score);
			Assert.Equal(string.Empty, team.TeamName);
			Assert.Equal(string.Empty, team.TeamTag);
			Assert.Equal(0, team.Winnings);
		}

		[Fact]
		public void WhenOpetingFilledMatchReport_ThenMatchReportWithCorrectValuesIsReturned()
		{
			var matchReport = _matchReportSerializer.ReadMatchReport(Path.Combine(TestFilesPath, "filledmatchreport.xlsx"));

			Assert.Equal("666", matchReport.MatchNumber);
			Assert.Equal("Play-Offs", matchReport.MatchType);
			Assert.Equal("e\nf\ng", matchReport.Report);

			Assert.Equal(-1, matchReport.HomeTeam.FAME);
			Assert.Equal(7, matchReport.HomeTeam.FansRoll);
			Assert.Equal(1, matchReport.HomeTeam.Gate);
			Assert.Equal(3, matchReport.HomeTeam.Inducement);
			Assert.NotEmpty(matchReport.HomeTeam.Inducements);
			Assert.Equal("b", matchReport.HomeTeam.Inducements[0]);
			Assert.Equal("c", matchReport.HomeTeam.Inducements[1]);
			Assert.Equal("d", matchReport.HomeTeam.Inducements[2]);
			Assert.Equal(1, matchReport.HomeTeam.MVP);
			Assert.Equal(2, matchReport.HomeTeam.PettyCash);
			Assert.Equal(9, matchReport.HomeTeam.Score);
			Assert.Equal("a", matchReport.HomeTeam.TeamName);
			Assert.Equal("a", matchReport.HomeTeam.TeamTag);
			Assert.Equal(1, matchReport.HomeTeam.Winnings);

			Assert.Equal(1, matchReport.AwayTeam.FAME);
			Assert.Equal(8, matchReport.AwayTeam.FansRoll);
			Assert.Equal(4, matchReport.AwayTeam.Gate);
			Assert.Equal(6, matchReport.AwayTeam.Inducement);
			Assert.NotEmpty(matchReport.AwayTeam.Inducements);
			Assert.Equal("i", matchReport.AwayTeam.Inducements[0]);
			Assert.Equal("j", matchReport.AwayTeam.Inducements[1]);
			Assert.Equal("k", matchReport.AwayTeam.Inducements[2]);
			Assert.Equal(2, matchReport.AwayTeam.MVP);
			Assert.Equal(5, matchReport.AwayTeam.PettyCash);
			Assert.Equal(10, matchReport.AwayTeam.Score);
			Assert.Equal("h", matchReport.AwayTeam.TeamName);
			Assert.Equal("h", matchReport.AwayTeam.TeamTag);
			Assert.Equal(2, matchReport.AwayTeam.Winnings);

			Assert.NotEmpty(matchReport.MatchActions);
			var matchActions = matchReport.MatchActions.ToArray();
			for (int i = 0; i < matchActions.Length; i++)
			{
				Assert.Equal("b" + (char)('a' + i), matchActions[i].Action);
				Assert.Equal((1 + i / 8).ToString(), matchActions[i].Half);
				Assert.Equal((i % 16) + 1, matchActions[i].SourcePlayer);
				Assert.Equal(((i+1) % 16) + 1, matchActions[i].TargetPlayer);
				Assert.Equal("a" + (char)('a' + i), matchActions[i].TeamTag);
				Assert.Equal((i % 8) + 1, matchActions[i].Turn);
			}
		}

		[Fact]
		public void WhenOpeningMatchReportFileThatDoesNotExists_ThenFileNotFoundExceptionIsThrown()
		{
			Assert.ThrowsDelegateWithReturn act = () => _matchReportSerializer.ReadMatchReport("foobar.xlsx");

			Assert.Throws<FileNotFoundException>(act);
		}

		private static string TestFilesPath
		{
			get
			{
				return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfiles");
			}
		}
	}
}
