using BloodBowl.DomainModel;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using Moq;
using Xunit;
using Xunit.Extensions;

namespace BloodBowl.MatchReport.UnitTest
{
	public class ParsedMatchActionTest
	{
		private readonly Mock<IMatchAction> _fakeAction;
		private readonly Mock<IRoster> _fakeHomeTeam;
		private readonly Mock<IRoster> _fakeAwayTeam;
		private readonly IPlayer[] _fakeHomePlayers;
		private readonly IPlayer[] _fakeAwayPlayers;

		public ParsedMatchActionTest()
		{
			_fakeHomePlayers = FakePlayers();
			_fakeAwayPlayers = FakePlayers();
			_fakeAction = new Mock<IMatchAction>(MockBehavior.Strict);
			_fakeHomeTeam = new Mock<IRoster>(MockBehavior.Strict);
			_fakeHomeTeam.Setup(x => x.TeamTag).Returns("HOME");
			_fakeHomeTeam.Setup(x => x.Players).Returns(_fakeHomePlayers);
			_fakeAwayTeam = new Mock<IRoster>(MockBehavior.Strict);
			_fakeAwayTeam.Setup(x => x.TeamTag).Returns("AWAY");
			_fakeAwayTeam.Setup(x => x.Players).Returns(_fakeAwayPlayers);
		}

		[Fact]
		public void Test_HomeTeamDoingTouchdown_ThenTypeIsTouchdownAndSourcePlayerIsCorrect()
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "HOME", "touchdown", 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Same(_fakeHomeTeam.Object, result.SourceTeam);
			Assert.Same(_fakeHomePlayers[4 - 1], result.SourcePlayer);
			Assert.Same(_fakeAwayTeam.Object, result.TargetTeam);
			Assert.Same(_fakeAwayPlayers[8 - 1], result.TargetPlayer);
		}

		[Fact]
		public void Test_AwayTeamDoingTouchdown_ThenTypeIsSourcePlayerIsAwayTeam()
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "AWAY", "touchdown", 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Same(_fakeAwayTeam.Object, result.SourceTeam);
			Assert.Same(_fakeAwayPlayers[4 - 1], result.SourcePlayer);
			Assert.Same(_fakeHomeTeam.Object, result.TargetTeam);
			Assert.Same(_fakeHomePlayers[8 - 1], result.TargetPlayer);
		}

		[InlineData("td")]
		[InlineData("TD")]
		[InlineData("touchdown")]
		[InlineData("Touchdown")]
		[Theory]
		public void WhenParsingTouchdown_ThenActionIsTouchdown(string matchAction)
		{
			ArrangeActionWithValidSourcePlayer("1st", 2, "HOME", matchAction, 4);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Equal(ParsedActionType.Touchdown, result.Type);
		}

		[InlineData("cas (11)")]
		[InlineData("CAS (12)")]
		[InlineData("casaulty (33)")]
		[InlineData("Casaulty (34)")]
		[InlineData("kill")]
		[InlineData("Kill")]
		[InlineData("kills")]
		[InlineData("Kills")]
		[Theory]
		public void WhenParsingCasaulty_ThenActionIsCasaulty(string matchAction)
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "HOME", matchAction, 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Equal(ParsedActionType.Casualty, result.Type);
		}

		[InlineData("kill")]
		[InlineData("kills")]
		[Theory]
		public void WhenParsingKills_ThenActionIsCasaulty(string matchAction)
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "HOME", matchAction, 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Equal(Injury.Dead, result.Injury);
		}

		[InlineData("cas (badly hurt)", Injury.BadlyHurt)]
		[InlineData("cas (11)", Injury.BadlyHurt)]
		[InlineData("cas (12)", Injury.BadlyHurt)]
		[InlineData("cas (13)", Injury.BadlyHurt)]
		[InlineData("cas (14)", Injury.BadlyHurt)]
		[InlineData("cas (15)", Injury.BadlyHurt)]
		[InlineData("cas (16)", Injury.BadlyHurt)]
		[InlineData("cas (17)", Injury.BadlyHurt)]
		[InlineData("cas (18)", Injury.BadlyHurt)]
		[InlineData("cas (21)", Injury.BadlyHurt)]
		[InlineData("cas (22)", Injury.BadlyHurt)]
		[InlineData("cas (23)", Injury.BadlyHurt)]
		[InlineData("cas (24)", Injury.BadlyHurt)]
		[InlineData("cas (25)", Injury.BadlyHurt)]
		[InlineData("cas (26)", Injury.BadlyHurt)]
		[InlineData("cas (27)", Injury.BadlyHurt)]
		[InlineData("cas (28)", Injury.BadlyHurt)]
		[InlineData("cas (31)", Injury.BadlyHurt)]
		[InlineData("cas (32)", Injury.BadlyHurt)]
		[InlineData("cas (33)", Injury.BadlyHurt)]
		[InlineData("cas (34)", Injury.BadlyHurt)]
		[InlineData("cas (35)", Injury.BadlyHurt)]
		[InlineData("cas (36)", Injury.BadlyHurt)]
		[InlineData("cas (37)", Injury.BadlyHurt)]
		[InlineData("cas (38)", Injury.BadlyHurt)]
		[InlineData("cas (Broken Ribs)", Injury.BrokenRibs)]
		[InlineData("cas (41)", Injury.BrokenRibs)]
		[InlineData("cas (Groin Strain)", Injury.GroinStrain)]
		[InlineData("cas (42)", Injury.GroinStrain)]
		[InlineData("cas (Gouged Eye)", Injury.GougedEye)]
		[InlineData("cas (43)", Injury.GougedEye)]
		[InlineData("cas (Broken Jaw)", Injury.BrokenJaw)]
		[InlineData("cas (44)", Injury.BrokenJaw)]
		[InlineData("cas (Fractured Arm)", Injury.FracturedArm)]
		[InlineData("cas (45)", Injury.FracturedArm)]
		[InlineData("cas (Fractured Leg)", Injury.FracturedLeg)]
		[InlineData("cas (46)", Injury.FracturedLeg)]
		[InlineData("cas (Smashed Hand)", Injury.SmashedHand)]
		[InlineData("cas (47)", Injury.SmashedHand)]
		[InlineData("cas (Pinched Nerve)", Injury.PinchedNerve)]
		[InlineData("cas (48)", Injury.PinchedNerve)]
		[InlineData("cas (Damaged Back)", Injury.DamagedBack)]
		[InlineData("cas (51)", Injury.DamagedBack)]
		[InlineData("cas (Smashed Knee)", Injury.SmashedKnee)]
		[InlineData("cas (52)", Injury.SmashedKnee)]
		[InlineData("cas (Smashed Hip)", Injury.SmashedHip)]
		[InlineData("cas (53)", Injury.SmashedHip)]
		[InlineData("cas (Smashed Ankle)", Injury.SmashedAnkle)]
		[InlineData("cas (54)", Injury.SmashedAnkle)]
		[InlineData("cas (Serious Concussion)", Injury.SeriousConcussion)]
		[InlineData("cas (55)", Injury.SeriousConcussion)]
		[InlineData("cas (Fractured Skull)", Injury.FracturedSkull)]
		[InlineData("cas (56)", Injury.FracturedSkull)]
		[InlineData("cas (Broken Neck)", Injury.BrokenNeck)]
		[InlineData("cas (57)", Injury.BrokenNeck)]
		[InlineData("cas (Smashed Collar Bone)", Injury.SmashedCollarBone)]
		[InlineData("cas (58)", Injury.SmashedCollarBone)]
		[InlineData("cas (Dead)", Injury.Dead)]
		[InlineData("cas (61)", Injury.Dead)]
		[InlineData("cas (62)", Injury.Dead)]
		[InlineData("cas (63)", Injury.Dead)]
		[InlineData("cas (64)", Injury.Dead)]
		[InlineData("cas (65)", Injury.Dead)]
		[InlineData("cas (66)", Injury.Dead)]
		[InlineData("cas (67)", Injury.Dead)]
		[InlineData("cas (68)", Injury.Dead)]
		[Theory]
		public void WhenParsingCasaultyWithInjury_ThenActionIsCasaulty(string matchAction, Injury expectedInjury)
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "HOME", matchAction, 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Equal(expectedInjury, result.Injury);
		}

		[InlineData("cas (68 -> badly hurt)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 11)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 12)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 13)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 14)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 15)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 16)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 17)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 18)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 21)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 22)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 23)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 24)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 25)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 26)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 27)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 28)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 31)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 32)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 33)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 34)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 35)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 36)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 37)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> 38)", Injury.BadlyHurt)]
		[InlineData("cas (68 -> Broken Ribs)", Injury.BrokenRibs)]
		[InlineData("cas (68 -> 41)", Injury.BrokenRibs)]
		[InlineData("cas (68 -> Groin Strain)", Injury.GroinStrain)]
		[InlineData("cas (68 -> 42)", Injury.GroinStrain)]
		[InlineData("cas (68 -> Gouged Eye)", Injury.GougedEye)]
		[InlineData("cas (68 -> 43)", Injury.GougedEye)]
		[InlineData("cas (68 -> Broken Jaw)", Injury.BrokenJaw)]
		[InlineData("cas (68 -> 44)", Injury.BrokenJaw)]
		[InlineData("cas (68 -> Fractured Arm)", Injury.FracturedArm)]
		[InlineData("cas (68 -> 45)", Injury.FracturedArm)]
		[InlineData("cas (68 -> Fractured Leg)", Injury.FracturedLeg)]
		[InlineData("cas (68 -> 46)", Injury.FracturedLeg)]
		[InlineData("cas (68 -> Smashed Hand)", Injury.SmashedHand)]
		[InlineData("cas (68 -> 47)", Injury.SmashedHand)]
		[InlineData("cas (68 -> Pinched Nerve)", Injury.PinchedNerve)]
		[InlineData("cas (68 -> 48)", Injury.PinchedNerve)]
		[InlineData("cas (68 -> Damaged Back)", Injury.DamagedBack)]
		[InlineData("cas (68 -> 51)", Injury.DamagedBack)]
		[InlineData("cas (68 -> Smashed Knee)", Injury.SmashedKnee)]
		[InlineData("cas (68 -> 52)", Injury.SmashedKnee)]
		[InlineData("cas (68 -> Smashed Hip)", Injury.SmashedHip)]
		[InlineData("cas (68 -> 53)", Injury.SmashedHip)]
		[InlineData("cas (68 -> Smashed Ankle)", Injury.SmashedAnkle)]
		[InlineData("cas (68 -> 54)", Injury.SmashedAnkle)]
		[InlineData("cas (68 -> Serious Concussion)", Injury.SeriousConcussion)]
		[InlineData("cas (68 -> 55)", Injury.SeriousConcussion)]
		[InlineData("cas (68 -> Fractured Skull)", Injury.FracturedSkull)]
		[InlineData("cas (68 -> 56)", Injury.FracturedSkull)]
		[InlineData("cas (68 -> Broken Neck)", Injury.BrokenNeck)]
		[InlineData("cas (68 -> 57)", Injury.BrokenNeck)]
		[InlineData("cas (68 -> Smashed Collar Bone)", Injury.SmashedCollarBone)]
		[InlineData("cas (68 -> 58)", Injury.SmashedCollarBone)]
		[InlineData("cas (68 -> Dead)", Injury.Dead)]
		[InlineData("cas (68 -> 61)", Injury.Dead)]
		[InlineData("cas (68 -> 62)", Injury.Dead)]
		[InlineData("cas (68 -> 63)", Injury.Dead)]
		[InlineData("cas (68 -> 64)", Injury.Dead)]
		[InlineData("cas (68 -> 65)", Injury.Dead)]
		[InlineData("cas (68 -> 66)", Injury.Dead)]
		[InlineData("cas (68 -> 67)", Injury.Dead)]
		[InlineData("cas (68 -> 68)", Injury.Dead)]
		[Theory]
		public void WhenParsingCasaultyWithInjuryAndApothecary_ThenActionIsCasaulty(string matchAction, Injury expectedApothecaryInjury)
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "HOME", matchAction, 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.Equal(expectedApothecaryInjury, result.ApothecaryInjury);
		}

		[InlineData("cas (68 -> regen)")]
		[InlineData("cas (68 -> regenerated)")]
		[InlineData("cas (68 -> 67 -> regen)")]
		[InlineData("cas (68 -> 67 -> regenerated)")]
		[Theory]
		public void WhenParsingCasaultyWithInjuryAndThePlayerRegenerated_ThenRegeneratedIsTrue(string matchAction)
		{
			ArrangeActionWithValidSourceAndTargetPlayer("1st", 2, "HOME", matchAction, 4, 8);

			var result = _fakeAction.Object.Parse(_fakeHomeTeam.Object, _fakeAwayTeam.Object, null);

			Assert.True(result.Regenerated);
		}

		private static IPlayer[] FakePlayers()
		{
			var players = new IPlayer[16];
			for (int i = 0; i < players.Length; i++)
			{
				players[i] = new Mock<IPlayer>().Object;
			}
			return players;
		}

		private void ArrangeActionWithValidSourceAndTargetPlayer(string half, int turn, string tag, string action, int sourcePlayer, int targetPlayer)
		{
			_fakeAction.Setup(x => x.Action).Returns(action);
			_fakeAction.Setup(x => x.Half).Returns(half);
			_fakeAction.Setup(x => x.Turn).Returns(turn);
			_fakeAction.Setup(x => x.SourcePlayer).Returns(sourcePlayer);
			_fakeAction.Setup(x => x.TargetPlayer).Returns(targetPlayer);
			_fakeAction.Setup(x => x.TeamTag).Returns(tag);
		}

		private void ArrangeActionWithValidTargetPlayer(string half, int turn, string tag, string action, int targetPlayer)
		{
			_fakeAction.Setup(x => x.Action).Returns(action);
			_fakeAction.Setup(x => x.Half).Returns(half);
			_fakeAction.Setup(x => x.Turn).Returns(turn);
			_fakeAction.Setup(x => x.SourcePlayer).Returns(0);
			_fakeAction.Setup(x => x.TargetPlayer).Returns(targetPlayer);
			_fakeAction.Setup(x => x.TeamTag).Returns(tag);
		}

		private void ArrangeActionWithValidSourcePlayer(string half, int turn, string tag, string action, int sourcePlayer)
		{
			_fakeAction.Setup(x => x.Action).Returns(action);
			_fakeAction.Setup(x => x.Half).Returns(half);
			_fakeAction.Setup(x => x.Turn).Returns(turn);
			_fakeAction.Setup(x => x.SourcePlayer).Returns(sourcePlayer);
			_fakeAction.Setup(x => x.TargetPlayer).Returns(0);
			_fakeAction.Setup(x => x.TeamTag).Returns(tag);
		}

		private void ArrangeActionWithoutPlayer(string half, int turn, string tag, string action)
		{
			_fakeAction.Setup(x => x.Action).Returns(action);
			_fakeAction.Setup(x => x.Half).Returns(half);
			_fakeAction.Setup(x => x.Turn).Returns(turn);
			_fakeAction.Setup(x => x.SourcePlayer).Returns(0);
			_fakeAction.Setup(x => x.TargetPlayer).Returns(0);
			_fakeAction.Setup(x => x.TeamTag).Returns(tag);
		}
	}
}
