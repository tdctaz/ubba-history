namespace BloodBowl.Core
{
	public static class StringExtensions
	{
		public static string SubstringUntil(this string value, char firstOf)
		{
			int index = value.IndexOfAny(new[] { firstOf });
			if (index == -1)
			{
				return value;
			}
			return value.Substring(0, index);
		}

		public static string ToStrippedLowerCase(this string value)
		{
			value = value.ToLowerInvariant();
			value = value.Replace(" ", "");
			value = value.Trim();
			return value;
		}
	}
}
