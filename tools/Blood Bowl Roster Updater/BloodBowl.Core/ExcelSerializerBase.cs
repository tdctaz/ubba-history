﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

namespace BloodBowl.Core
{
	public class ExcelSerializerBase : IDisposable
	{
		private bool _disposed = false;
		private readonly Application _application;
		private readonly Workbooks _workbooks;

		public ExcelSerializerBase()
		{
			_application = new Application();
			_workbooks = _application.Workbooks;
		}

		~ExcelSerializerBase()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
				}

				if (_workbooks != null)
				{
					_workbooks.Close();
					ReleaseComObject(_workbooks);
				}

				_application.Quit();
				ReleaseComObject(_application);

				_disposed = true;
			}
		}

		protected Workbooks Workbooks
		{
			get
			{
				return _workbooks;
			}
		}

		protected static void ReleaseComObject(object obj)
		{
			if (obj == null)
			{
				return;
			}
			try
			{
				Marshal.ReleaseComObject(obj);
			}
			catch (Exception)
			{
			}
			finally
			{
				GC.Collect();
				GC.WaitForPendingFinalizers();
				GC.Collect();
				GC.WaitForPendingFinalizers();
			}
		}

		protected void SetValue(Worksheet sheet, string cell, string value, string nullValue = null)
		{
			if (value == nullValue)
			{
				value = null;
			}
			sheet.Range[cell].Value2 = value;
		}

		protected int GetAsInt(Worksheet sheet, string cell, int emptyValue = 0)
		{
			var value = sheet.Range[cell].Value2;
			if (value == null)
			{
				return emptyValue;
			}
			string strValue = value.ToString();
			strValue = strValue.Trim(new[] { ' ', '\t', '#' });
			strValue = strValue.Replace(" ", "");
			if (strValue == string.Empty || strValue == "-" || strValue == "?")
			{
				return emptyValue;
			}
			try
			{
				return int.Parse(strValue, NumberStyles.Any);
			}
			catch(FormatException exception)
			{
				throw new ExcelCellFormatException("Unable to parse '" + value.ToString() + "' as integer", cell, exception);
			}
		}

		protected short GetAsShort(Worksheet sheet, string cell, short emptyValue = (short)0)
		{
			var value = sheet.Range[cell].Value2;
			if (value == null)
			{
				return emptyValue;
			}
			string strValue = value.ToString();
			strValue = strValue.Trim(new[] { ' ', '\t', '#' });
			strValue = strValue.Replace(" ", "");
			if (strValue == string.Empty || strValue == "-" || strValue == "?")
			{
				return emptyValue;
			}
			try
			{
				return short.Parse(strValue, NumberStyles.Any);
			}
			catch (FormatException exception)
			{
				throw new ExcelCellFormatException("Unable to parse '" + value.ToString() + "' as short", cell, exception);
			}
		}

		protected bool GetAsBool(Worksheet sheet, string cell, bool emptyValue = false)
		{
			var value = sheet.Range[cell].Value2;
			if (value == null)
			{
				return emptyValue;
			}
			string strValue = value.ToString();
			strValue = strValue.Trim(new[] { ' ', '\t', '#' });
			strValue = strValue.Replace(" ", "");
			return strValue == "true" || strValue == "x" || strValue == "X" || strValue == "1";
		}

		protected double GetAsDouble(Worksheet sheet, string cell, double emptyValue = 0.0)
		{
			var value = sheet.Range[cell].Value2;
			if (value == null)
			{
				return emptyValue;
			}
			string strValue = value.ToString();
			strValue = strValue.Trim(new[] { ' ', '\t', '#' });
			strValue = strValue.Replace(" ", "");
			if (strValue == string.Empty || strValue == "-" || strValue == "?")
			{
				return emptyValue;
			}
			try
			{
				return double.Parse(strValue, NumberStyles.Any);
			}
			catch (FormatException exception)
			{
				throw new ExcelCellFormatException("Unable to parse '" + value.ToString() + "' as double", cell, exception);
			}
		}

		protected string GetAsString(Worksheet sheet, string cell)
		{
			var value = sheet.Range[cell].Value2;
			if (value == null)
			{
				return string.Empty;
			}
			return value.ToString();
		}

		protected string[] GetAsMultilinedString(Worksheet sheet, string cell)
		{
			var value = sheet.Range[cell].Value2;
			if (value == null)
			{
				return new string[0];
			}
			return value.ToString().Split(new[] { '\n' });
		}
	}
}
