using System;
using System.Runtime.Serialization;

namespace BloodBowl.Core
{
	public class ExcelCellFormatException : FormatException
	{
		public ExcelCellFormatException()
		{
		}

		public ExcelCellFormatException(string message, string cell)
				: base(message)
		{
			Cell = cell;
		}

		public ExcelCellFormatException(string message, string cell, Exception innerException)
				: base(message, innerException)
		{
			Cell = cell;
		}

		protected ExcelCellFormatException(SerializationInfo info, StreamingContext context)
				: base(info, context)
		{
		}

		public string Cell { get; set; }
	}
}
