using System.Collections.Generic;

namespace BloodBowl.Core
{
	public static class RangeExtensions
	{
		public static IEnumerable<int> RangeTo(this int min, int max)
		{
			for (int i = min; i < max+1; i++)
			{
				yield return i;
			}
		}
	}
}
