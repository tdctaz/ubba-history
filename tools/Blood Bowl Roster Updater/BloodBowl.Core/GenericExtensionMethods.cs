namespace BloodBowl.Core
{
	public static class GenericExtensionMethods
	{
		public static T As<T>(this object instance) where T : class
		{
			return instance as T;
		}
	}
}
