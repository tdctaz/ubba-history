using BloodBowl.RosterUpdator.RosterAction;
using BloodBowl.RosterUpdator.Statistics;
using log4net;
using StructureMap;

namespace BloodBowl.RosterUpdator
{
	public static class StructureMapBootstrapper
	{
		public static void Initialize(string[] args)
		{
			ObjectFactory.Initialize(x =>
			{
				x.Scan(scanner =>
				{
					scanner.TheCallingAssembly();
					scanner.WithDefaultConventions();
					scanner.AddAllTypesOf<IApplyAction>();
					scanner.AddAllTypesOf<IStatistics>();
				});
				x.For<ILog>().Use(() => LogManager.GetLogger(typeof(Program)));
				x.For<IProgramArguments>().Singleton().Use(() => new ProgramArguments(args));
				x.ForConcreteType<RosterReader>();
				x.ForConcreteType<MatchReportReader>();
			});
		}
	}
}
