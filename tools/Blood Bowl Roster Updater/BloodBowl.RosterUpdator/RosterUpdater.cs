using System;
using System.Linq;
using BloodBowl.Core;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using BloodBowl.RosterUpdator.RosterAction;
using log4net;
using StructureMap;

namespace BloodBowl.RosterUpdator
{
	public class MatchRosterUpdater
	{
		public MatchRosterUpdater(ILog log)
		{
			Log = log;
		}

		protected ILog Log { get; private set; }

		public void CleanUpRosterPreUpdate(Roster.Roster roster)
		{
			foreach (var player in roster.Players.Where(x => x.MissNextMatch))
			{
				player.As<Player>().MissNextMatch = false;
			}
		}

		public void CleanUpRosterPostUpdate(Roster.Roster roster)
		{
			foreach (IPlayer player in roster.Players.Where(x => x.IsJourneyPlayer))
			{
				ApplyRemovePlayer(roster, player.Number);
			}
		}

		public void UpdateActions(ParsedMatchAction action)
		{
			var actionTypes = ObjectFactory.GetAllInstances<IApplyAction>().ToDictionary(x => x.HandledActionType);
			actionTypes[action.Type].ApplyAction(action);
		}

		public void UpdateRoster(Roster.Roster roster, ITeamReport report, int opponentScore)
		{
			roster.Treasury -= report.PettyCash;
			roster.Treasury += report.Winnings;
			roster.Players[report.MVP-1].As<Player>().MVPs++;
			if (opponentScore >= report.Score && report.FansRoll < roster.FanFactor)
			{
				roster.FanFactor--;
			}
			if (opponentScore <= report.Score && report.FansRoll > roster.FanFactor)
			{
				roster.FanFactor++;
			}
		}

		public void UpdateJourneyPlayers(Roster.Roster roster)
		{
			int missingPlayers = 11 - roster.CountActivePlayers();
			for (int i = 0; i < missingPlayers; i++)
			{
				var emptyPlayer = roster.Players.Reverse().Where(x => x.IsEmpty).FirstOrDefault();
				if (emptyPlayer == null)
				{
					continue;
				}
				IPlayerDetails journeymanPlayerDetails = roster.Details.PlayerDetails.Where(x => x.Journeyman).First();
				var journeymanPlayer = new Player
				{
						Type = journeymanPlayerDetails.TypeName,
						Name = "<noname " + (i + 1) + ">",
				};
				ApplyAddPlayer(roster, journeymanPlayer, emptyPlayer.Number);
			}
		}

		private void ApplyRemovePlayer(Roster.Roster roster, int number)
		{
			if (number < 1 || number > 16)
			{
				throw new ArgumentException("", "number");
			}

			Log.DebugFormat("Remove player {0} from {1}'s roster", roster.Players[number - 1], roster.TeamName);
			ApplyAddPlayer(roster, new Player(), number);
		}

		protected void ApplyAddPlayer(Roster.Roster roster, Player player, int number)
		{
			if (number < 1 || number > 16)
			{
				throw new ArgumentException("", "number");
			}
			if (player.IsEmpty == false)
			{
				Log.DebugFormat("Added player {0} to {1}'s roster", player, roster.TeamName);
			}

			player.Number = number;
			roster.Players[number - 1] = player;
		}
	}
}
