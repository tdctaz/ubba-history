﻿using System;
using log4net;
using StructureMap;

namespace BloodBowl.RosterUpdator
{
	class Program
	{
		static void Main(string[] args)
		{
			StructureMapBootstrapper.Initialize(args);
			var log = ObjectFactory.GetInstance<ILog>();
			try
			{
				log.Info("Starting...");

				var rosters = ObjectFactory.GetInstance<RosterReader>().Rosters;
				var matchreportReader = ObjectFactory.GetInstance<MatchReportReader>();
				matchreportReader.ParseMatchReports(rosters);

				log.Info("Done!");
			}
			catch(Exception exception)
			{
				log.Fatal("Failure!", exception);
			}

			Console.WriteLine("Press any to to quit!");
			Console.ReadKey(intercept: true);
		}
	}
}
