﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator
{
	public class RosterReader
	{
		private readonly Lazy<Dictionary<string, Roster.Roster>> _readRosters;

		public RosterReader(IProgramArguments arguments, ILog log)
		{
			Log = log;
			_readRosters = new Lazy<Dictionary<string, Roster.Roster>>(() => ReadRosters(arguments.RosterLocation));
		}

		public Dictionary<string, Roster.Roster> Rosters
		{
			get
			{
				return _readRosters.Value;
			}
		}

		public ILog Log { get; private set; }

		private Dictionary<string, Roster.Roster> ReadRosters(DirectoryInfo rosterLocation)
		{
			var currentRosters = new Dictionary<string, Roster.Roster>();
			var startRostersDirectory = new DirectoryInfo(rosterLocation.FullName);
			var startRostersFiles = startRostersDirectory.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly).Where(x => x.Name.StartsWith("~") == false).OrderBy(x => x.Name);
			using (var rosterSerializer = new RosterSerializer())
			{
				foreach (var startRostersFile in startRostersFiles)
				{
					Log.InfoFormat("Reading roster '{0}'", startRostersFile.Name);
					var roster = rosterSerializer.ReadRoster(startRostersFile.FullName);
					currentRosters[roster.TeamName] = (Roster.Roster)roster;
				}
			}
			return currentRosters;
		}
	}
}
