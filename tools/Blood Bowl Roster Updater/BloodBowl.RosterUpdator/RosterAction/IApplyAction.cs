using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.RosterAction
{
	public interface IApplyAction
	{
		void ApplyAction(ParsedMatchAction action);

		ParsedActionType HandledActionType { get; }
	}
}
