using BloodBowl.DomainModel;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction
{
	public abstract class ApplyInjuryActionBase : ApplyActionBase
	{
		protected ApplyInjuryActionBase(ILog log, ParsedActionType handledActionType)
				: base(log, handledActionType)
		{
		}

		protected InjuryType ApplyInjuryToPlayer(Roster.Roster roster, Player player, Injury injury, Injury apothecaryInjury, bool regenerated)
		{
			if (regenerated)
			{
				return InjuryType.None;
			}
			if (apothecaryInjury != Injury.None)
			{
				InjuryType injuryType = apothecaryInjury.GetInjuryType();
				ApplyInjuryToPlayer(roster, player, injuryType);
				return injuryType;
			}
			if (injury != Injury.None)
			{
				InjuryType injuryType = injury.GetInjuryType();
				ApplyInjuryToPlayer(roster, player, injuryType);
				return injuryType;
			}
			return InjuryType.None;
		}

		protected void ApplyInjuryToPlayer(Roster.Roster roster, Player player, InjuryType injuryType)
		{
			if (GetPlayerStatus(player).HasAnyFlag(PlayerStatus.Active | PlayerStatus.Inactive) == false)
			{
				Log.WarnFormat("Injury: {0} will NOT be applied to temporary player {1}", injuryType, player);
				return;
			}

			switch (injuryType)
			{
			case InjuryType.MissNextGame:
				Log.DebugFormat("{0} will miss next game", player);
				player.MissNextMatch = true;
				break;
			case InjuryType.NigglingInjury:
				Log.DebugFormat("{0} will miss next game and Niggling Injury", player);
				player.MissNextMatch = true;
				player.NigglingInjury++;
				break;
			case InjuryType.ReducedMovement:
				Log.DebugFormat("{0} will miss next game and -MA", player);
				player.MissNextMatch = true;
				player.InjuriesMovement--;
				break;
			case InjuryType.ReducedStrength:
				Log.DebugFormat("{0} will miss next game and -ST", player);
				player.MissNextMatch = true;
				player.InjuriesStrength--;
				break;
			case InjuryType.ReducedAgility:
				Log.DebugFormat("{0} will miss next game and -AG", player);
				player.MissNextMatch = true;
				player.InjuriesAgility--;
				break;
			case InjuryType.ReducedArmor:
				Log.DebugFormat("{0} will miss next game and -AV", player);
				player.MissNextMatch = true;
				player.InjuriesArmor--;
				break;
			case InjuryType.Dead:
				Log.DebugFormat("{0} is DEAD!", player);
				ApplyRemovePlayer(roster, player.Number);
				break;
			}
		}
	}
}
