using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyPostmatchFirePlayer : ApplyActionBase
	{
		public ApplyPostmatchFirePlayer(ILog log)
				: base(log, ParsedActionType.Fire)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var player = action.SourcePlayer.As<Player>();
			if (GetPlayerStatus(player).HasAnyFlag(PlayerStatus.Active | PlayerStatus.Inactive))
			{
				ApplyRemovePlayer(action.SourceTeam.As<Roster.Roster>(), player.Number);
			}
			else
			{
				Log.WarnFormat("Cannot apply '{0}' to non-permanent player {1}.", action, player);
			}
		}
	}
}
