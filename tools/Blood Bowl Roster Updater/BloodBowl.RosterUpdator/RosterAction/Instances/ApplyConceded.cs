using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyConceded : ApplyActionBase
	{
		public ApplyConceded(ILog log)
				: base(log, ParsedActionType.Conceded)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var concededTeam = action.SourceTeam.As<Roster.Roster>();
			var concededMvp = action.SourcePlayer.As<Player>();
			int concededMoney = action.Price;

			concededTeam.Treasury -= concededMoney;
			concededMvp.MVPs--;

			var winningTeam = action.TargetTeam.As<Roster.Roster>();
			winningTeam.Treasury += concededMoney;
			winningTeam.Players[concededMvp.Number - 1].As<Player>().MVPs++;
		}
	}
}
