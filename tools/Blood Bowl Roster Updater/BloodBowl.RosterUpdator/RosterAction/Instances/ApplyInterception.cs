using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyInterception : ApplyActionBase
	{
		public ApplyInterception(ILog log)
				: base(log, ParsedActionType.Interception)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var player = action.TargetPlayer.As<Player>();
			if (GetPlayerStatus(player).HasAnyFlag(PlayerStatus.Active))
			{
				player.ActInterceptions++;
			}
			else
			{
				Log.WarnFormat("Cannot apply '{0}' to a non-active player {1}.", action, player);
			}
		}
	}
}
