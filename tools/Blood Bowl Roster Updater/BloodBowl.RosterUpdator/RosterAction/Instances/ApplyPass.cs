using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyPass : ApplyActionBase
	{
		public ApplyPass(ILog log)
				: base(log, ParsedActionType.Pass)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var player = action.SourcePlayer.As<Player>();
			if (GetPlayerStatus(player).HasAnyFlag(PlayerStatus.Active))
			{
				player.ActCompletions++;
			}
			else
			{
				Log.WarnFormat("Cannot apply '{0}' to a non-active player {1}.", action, player);
			}
		}
	}
}
