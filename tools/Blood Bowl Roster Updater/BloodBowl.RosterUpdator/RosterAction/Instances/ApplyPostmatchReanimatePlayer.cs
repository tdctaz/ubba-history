using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyPostmatchReanimatePlayer : ApplyActionBase
	{
		public ApplyPostmatchReanimatePlayer(ILog log)
				: base(log, ParsedActionType.Reanimate)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var newPlayer = new Player
			{
				Type = action.PlayerType,
				Name = action.Name,
				Number = action.SourcePlayer.Number,
			};
			ApplyAddPlayer(action.SourceTeam.As<Roster.Roster>(), newPlayer, action.SourcePlayer.Number);
		}
	}
}
