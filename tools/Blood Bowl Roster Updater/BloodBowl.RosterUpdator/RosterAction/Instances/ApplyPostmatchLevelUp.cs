using BloodBowl.Core;
using BloodBowl.DomainModel;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyPostmatchLevelUp : ApplyActionBase
	{
		public ApplyPostmatchLevelUp(ILog log)
				: base(log, ParsedActionType.LevelUp)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			if (GetPlayerStatus(action.SourcePlayer).HasAnyFlag(PlayerStatus.Active | PlayerStatus.Inactive))
			{
				ApplyLevelUp(action.SourcePlayer.As<Player>(), action.Skill);
			}
			else
			{
				Log.WarnFormat("Cannot apply '{0}' to a non-permanent player.", action);
			}
		}

		private void ApplyLevelUp(Player player, SkillType skill)
		{
			if (string.IsNullOrEmpty(player.Upgrade1))
			{
				player.Upgrade1 = skill.ToRosterString();
			}
			else if (string.IsNullOrEmpty(player.Upgrade2))
			{
				player.Upgrade2 = skill.ToRosterString();
			}
			else if (string.IsNullOrEmpty(player.Upgrade3))
			{
				player.Upgrade2 = skill.ToRosterString();
			}
			else if (string.IsNullOrEmpty(player.Upgrade4))
			{
				player.Upgrade3 = skill.ToRosterString();
			}
			else if (string.IsNullOrEmpty(player.Upgrade5))
			{
				player.Upgrade4 = skill.ToRosterString();
			}
			else if (string.IsNullOrEmpty(player.Upgrade6))
			{
				player.Upgrade6 = skill.ToRosterString();
			}
			else
			{
				Log.ErrorFormat("Cannot level up {0} the player already has 6 skills", player);
			}
		}
	}
}
