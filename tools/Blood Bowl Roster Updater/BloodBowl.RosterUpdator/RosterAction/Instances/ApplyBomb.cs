using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyBomb : ApplyInjuryActionBase
	{
		public ApplyBomb(ILog log)
			: base(log, ParsedActionType.Bomb)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			ApplyInjuryToPlayer(action.TargetTeam.As<Roster.Roster>(), action.TargetPlayer.As<Player>(), action.Injury, action.ApothecaryInjury, action.Regenerated);
		}
	}

	public class ApplyThrowRock : ApplyInjuryActionBase
	{
		public ApplyThrowRock(ILog log)
			: base(log, ParsedActionType.ThrowRock)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			ApplyInjuryToPlayer(action.TargetTeam.As<Roster.Roster>(), action.TargetPlayer.As<Player>(), action.Injury, action.ApothecaryInjury, action.Regenerated);
		}
	}
}
