using BloodBowl.Core;
using BloodBowl.DomainModel;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyCasaulty : ApplyInjuryActionBase
	{
		public ApplyCasaulty(ILog log)
				: base(log, ParsedActionType.Casualty)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var injury = ApplyInjuryToPlayer(action.TargetTeam.As<Roster.Roster>(), action.TargetPlayer.As<Player>(), action.Injury, action.ApothecaryInjury, action.Regenerated);

			if (GetPlayerStatus(action.SourcePlayer).HasAnyFlag(PlayerStatus.Active | PlayerStatus.Inactive))
			{
				action.SourcePlayer.As<Player>().ActCasaulties++;
				if (injury == InjuryType.Dead)
				{
					action.SourcePlayer.As<Player>().ActKills++;
				}
			}
			else
			{
				Log.WarnFormat("Cannot apply '{0}' to a non-permanent player {1}.", action, action.SourcePlayer);
			}
		}
	}
}
