using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyEat : ApplyInjuryActionBase
	{
		public ApplyEat(ILog log)
				: base(log, ParsedActionType.Eat)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			ApplyInjuryToPlayer(action.TargetTeam.As<Roster.Roster>(), action.TargetPlayer.As<Player>(), action.Injury, action.ApothecaryInjury, action.Regenerated);
		}
	}
}
