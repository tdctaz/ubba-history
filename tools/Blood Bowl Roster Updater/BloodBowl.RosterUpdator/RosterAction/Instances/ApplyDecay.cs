using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyDecay : ApplyInjuryActionBase
	{
		public ApplyDecay(ILog log)
				: base(log, ParsedActionType.Decay)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			ApplyInjuryToPlayer(action.SourceTeam.As<Roster.Roster>(), action.SourcePlayer.As<Player>(), action.Injury, action.ApothecaryInjury, action.Regenerated);
		}
	}
}
