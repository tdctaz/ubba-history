using BloodBowl.Core;
using BloodBowl.MatchReport;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyPostmatchPurchase : ApplyActionBase
	{
		public ApplyPostmatchPurchase(ILog log)
				: base(log, ParsedActionType.Purchase)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var roster = action.SourceTeam.As<Roster.Roster>();
			ApplyPurchase(roster, action.Item);
			roster.Treasury -= action.Price;
		}

		private static void ApplyPurchase(Roster.Roster roster, ItemType item)
		{
			switch (item)
			{
			case ItemType.Apothecary:
				roster.Apothecary++;
				break;
			case ItemType.AssCoaches:
				roster.AssCoaches++;
				break;
			case ItemType.Cheerleader:
				roster.Cheerleaders++;
				break;
			case ItemType.ReRoll:
				roster.ReRolls++;
				break;
			}
		}
	}
}
