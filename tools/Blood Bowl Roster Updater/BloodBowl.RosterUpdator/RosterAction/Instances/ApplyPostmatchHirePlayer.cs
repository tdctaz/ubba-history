using BloodBowl.Core;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction.Instances
{
	public class ApplyPostmatchHireJourneyman : ApplyActionBase
	{
		public ApplyPostmatchHireJourneyman(ILog log)
			: base(log, ParsedActionType.HireJourneyman)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var player = action.SourceTeam.As<Roster.Roster>().Players[action.SourcePlayer.Number - 1].As<Player>();
			player.Type = action.PlayerType;
			player.Name = action.Name;
			action.SourceTeam.As<Roster.Roster>().Treasury -= action.Price;
		}
	}

	public class ApplyPostmatchHirePlayer : ApplyActionBase
	{
		public ApplyPostmatchHirePlayer(ILog log)
				: base(log, ParsedActionType.Hire)
		{
		}

		protected override void InternalApplyAction(ParsedMatchAction action)
		{
			var newPlayer = new Player
			{
				Type = action.PlayerType,
				Name = action.Name,
				Number = action.SourcePlayer.Number,
			};
			ApplyAddPlayer(action.SourceTeam.As<Roster.Roster>(), newPlayer, action.SourcePlayer.Number);
			action.SourceTeam.As<Roster.Roster>().Treasury -= action.Price;
		}
	}
}
