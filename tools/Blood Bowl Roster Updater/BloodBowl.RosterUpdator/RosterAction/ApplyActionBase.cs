using System;
using BloodBowl.DomainModel;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using log4net;

namespace BloodBowl.RosterUpdator.RosterAction
{
	public abstract class ApplyActionBase : IApplyAction
	{
		protected ApplyActionBase(ILog log, ParsedActionType handledActionType)
		{
			Log = log;
			HandledActionType = handledActionType;
		}

		public void ApplyAction(ParsedMatchAction action)
		{
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			if (action.Type != HandledActionType)
			{
				throw new InvalidOperationException("A handler for '" + HandledActionType + "' cannot handle an action of the type '" + action.Type + "'");
			}

			Log.DebugFormat("Applying action: {0}", action);
			InternalApplyAction(action);
		}

		public ParsedActionType HandledActionType
		{
			get;
			private set;
		}

		protected ILog Log { private set; get; }

		protected abstract void InternalApplyAction(ParsedMatchAction matchAction);

		protected PlayerStatus GetPlayerStatus(IPlayer player)
		{
			if (player == null)
			{
				throw new ArgumentNullException("player", "Cannot determine status of a player which is null");
			}

			PlayerStatus status = PlayerStatus.None;
			if (player.IsEmpty)
			{
				return status;
			}
			if (player.IsJourneyPlayer)
			{
				status |= PlayerStatus.Journey;
			}
			if (player.MissNextMatch)
			{
				status |= PlayerStatus.Inactive;
			}
			else
			{
				status |= PlayerStatus.Active;
			}
			return status;
		}

		protected void ApplyRemovePlayer(Roster.Roster roster, int number)
		{
			if (number < 1 || number > 16)
			{
				throw new ArgumentException("", "number");
			}

			Log.DebugFormat("Remove player {0} from {1}'s roster", roster.Players[number - 1], roster.TeamName);
			ApplyAddPlayer(roster, new Player(), number);
		}

		protected void ApplyAddPlayer(Roster.Roster roster, Player player, int number)
		{
			if (number < 1 || number > 16)
			{
				throw new ArgumentException("", "number");
			}
			if (player.IsEmpty == false)
			{
				Log.DebugFormat("Added player {0} to {1}'s roster", player, roster.TeamName);
			}

			player.Number = number;
			roster.Players[number - 1] = player;
		}

		[Flags]
		public enum PlayerStatus
		{
			None = 0,
			Journey = 1,
			Inactive = 2,
			Active = 4,
		}
	}

	public static class PlayerStatusExtensions
	{
		public static bool HasAnyFlag(this ApplyActionBase.PlayerStatus status, ApplyActionBase.PlayerStatus flags)
		{
			return (status & flags) > 0;
		}
	}
}
