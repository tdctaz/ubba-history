using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BloodBowl.MatchReport;
using BloodBowl.Roster;
using BloodBowl.RosterUpdator.Statistics;
using log4net;
using StructureMap;

namespace BloodBowl.RosterUpdator
{
	public class MatchReportReader
	{
		private readonly DirectoryInfo _matchReportsLocation;
		private readonly DirectoryInfo _outputDirectory;

		public MatchReportReader(IProgramArguments arguments, ILog log)
		{
			Log = log;
			_matchReportsLocation = arguments.MatchReportsLocation;
			_outputDirectory = arguments.OutputDirectory;
		}

		public ILog Log { get; private set; }

		public void ParseMatchReports(Dictionary<string, Roster.Roster> currentRosters)
		{
			var statistics = ObjectFactory.GetAllInstances<IStatistics>();

			var matchReportsDirectory = new DirectoryInfo(_matchReportsLocation.FullName);
			var matchReportFiles = matchReportsDirectory.GetFiles("*.xlsx", SearchOption.TopDirectoryOnly).Where(x => x.Name.StartsWith("~") == false).OrderBy(x => x.Name);
			var matchUpdater = new MatchRosterUpdater(ObjectFactory.GetInstance<ILog>());
			int matchNumber = 0;
			using (var matchReportSerializer = new MatchReportSerializer())
			{
				foreach (var matchReportFile in matchReportFiles)
				{
					Console.WriteLine("Reading match report '{0}'", matchReportFile.Name);
					var matchReport = matchReportSerializer.ReadMatchReport(matchReportFile.FullName);
					var homeTeam = currentRosters[matchReport.HomeTeam.TeamName];
					var awayTeam = currentRosters[matchReport.AwayTeam.TeamName];
					statistics.StatisticsMatch(homeTeam, awayTeam, matchReport);

					matchUpdater.CleanUpRosterPreUpdate(homeTeam);
					matchUpdater.CleanUpRosterPreUpdate(awayTeam);
					matchUpdater.UpdateRoster(homeTeam, matchReport.HomeTeam, matchReport.AwayTeam.Score);
					matchUpdater.UpdateRoster(awayTeam, matchReport.AwayTeam, matchReport.HomeTeam.Score);
					foreach (var matchAction in matchReport.MatchActions)
					{
						if (matchAction.IsEmpty())
						{
							continue;
						}
						var action = matchAction.Parse(homeTeam, awayTeam, matchReport);
						matchUpdater.UpdateActions(action);
						statistics.StatisticsAction(homeTeam, awayTeam, action);
					}
					matchUpdater.CleanUpRosterPostUpdate(homeTeam);
					matchUpdater.CleanUpRosterPostUpdate(awayTeam);
					matchUpdater.UpdateJourneyPlayers(homeTeam);
					matchUpdater.UpdateJourneyPlayers(awayTeam);
					matchNumber++;

					using (var rosterSerializer = new RosterSerializer())
					{
						rosterSerializer.WriteRoster(Path.Combine(_outputDirectory.FullName, "Team - " + homeTeam.TeamName + " - aftermatch " + matchNumber + ".xlsx"), homeTeam);
						rosterSerializer.WriteRoster(Path.Combine(_outputDirectory.FullName, "Team - " + awayTeam.TeamName + " - aftermatch " + matchNumber + ".xlsx"), awayTeam);
					}
				}
			}
			statistics.StatisticsPrint();
		}
	}
}
