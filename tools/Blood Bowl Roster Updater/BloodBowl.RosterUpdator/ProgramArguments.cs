﻿using System.Collections.Generic;
using System.IO;
using Mono.Options;

namespace BloodBowl.RosterUpdator
{
	public interface IProgramArguments
	{
		DirectoryInfo RosterLocation { get; }
		DirectoryInfo MatchReportsLocation { get; }
		DirectoryInfo OutputDirectory { get; }
	}

	public class ProgramArguments : IProgramArguments
	{
		public ProgramArguments(string[] args)
		{
			var optionset = new OptionSet
			{
					{"ro|rosters=", "Location of the input rosters", x => RosterLocation = new DirectoryInfo(x)},
					{"mr|matchreports|reports=", "Location of the match reports", x => MatchReportsLocation = new DirectoryInfo(x)},
					{"o|output=", "Output folder", x => OutputDirectory = new DirectoryInfo(x)},
			};
			optionset.Parse(args);
		}

		public DirectoryInfo RosterLocation { get; private set; }
		public DirectoryInfo MatchReportsLocation { get; private set; }
		public DirectoryInfo OutputDirectory { get; private set; }
	}
}
