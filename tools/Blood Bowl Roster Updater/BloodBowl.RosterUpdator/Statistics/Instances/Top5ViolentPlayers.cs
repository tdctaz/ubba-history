using System.Linq;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics.Instances
{
	public class Top5MostViolentPlayers : PlayerCounterBase
	{
		public override void HandleMatch(IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport)
		{
		}

		public override void HandleAction(IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action)
		{
			switch (action.Type)
			{
			case ParsedActionType.Casualty:
				AddPoints(action.SourcePlayer, 1);
				break;
			}
		}

		public override string ToString()
		{
			return GetTop(5).Aggregate("\n\nTop 5 Most violent players\n", (current, playerSpp) => current + string.Format("{0} - {1}\n", playerSpp.Key, playerSpp.Value));
		}
	}
}
