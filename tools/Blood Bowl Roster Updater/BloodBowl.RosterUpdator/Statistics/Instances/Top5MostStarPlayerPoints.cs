using System.Linq;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics.Instances
{
	public class Top5MostStarPlayerPoints : PlayerCounterBase
	{
		public override void HandleMatch(IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport)
		{
			AddPoints(homeTeam.Players[matchReport.HomeTeam.MVP-1], 5);
			AddPoints(awayTeam.Players[matchReport.AwayTeam.MVP-1], 5);
		}

		public override void HandleAction(IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action)
		{
			switch (action.Type)
			{
			case ParsedActionType.Interception:
				AddPoints(action.TargetPlayer, 2);
				break;
			case ParsedActionType.Casualty:
				AddPoints(action.SourcePlayer, 2);
				break;
			case ParsedActionType.Touchdown:
				AddPoints(action.SourcePlayer, 3);
				break;
			case ParsedActionType.Pass:
				AddPoints(action.SourcePlayer, 1);
				break;
			}
		}

		public override string ToString()
		{
			return GetTop(5).Aggregate("\n\nTop 5 Stars (most star player points)\n", (current, playerSpp) => current + string.Format("{0} - {1}\n", playerSpp.Key, playerSpp.Value));
		}
	}
}
