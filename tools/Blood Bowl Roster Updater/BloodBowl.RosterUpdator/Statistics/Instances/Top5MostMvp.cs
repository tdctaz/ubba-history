using System.Linq;
using BloodBowl.DomainModel;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics.Instances
{
	public class Top5MostMvp : PlayerCounterBase
	{
		public override void HandleMatch(IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport)
		{
			AddPoints(homeTeam.Players[matchReport.HomeTeam.MVP - 1], 1);
			AddPoints(awayTeam.Players[matchReport.AwayTeam.MVP - 1], 1);
		}

		public override void HandleAction(IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action)
		{
		}

		public override string ToString()
		{
			return GetTop(5).Aggregate("\n\nTop 5 Most MVPs\n", (current, playerSpp) => current + string.Format("{0} - {1}\n", playerSpp.Key, playerSpp.Value));
		}
	}
}
