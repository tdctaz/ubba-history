using System.Linq;
using BloodBowl.DomainModel;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics.Instances
{
	public class Top5Killers : PlayerCounterBase
	{
		public override void HandleMatch(IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport)
		{
		}

		public override void HandleAction(IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action)
		{
			switch (action.Type)
			{
			case ParsedActionType.Casualty:
				if (action.Regenerated == false && action.Injury == Injury.Dead)
				{
					AddPoints(action.SourcePlayer, 1);
				}
				break;
			case ParsedActionType.Eat:
				AddPoints(action.SourcePlayer, 1);
				break;
			}
		}

		public override string ToString()
		{
			return GetTop(5).Aggregate("\n\nTop 5 Killers\n", (current, playerSpp) => current + string.Format("{0} - {1}\n", playerSpp.Key, playerSpp.Value));
		}
	}
}
