﻿using System;
using System.Collections.Generic;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics
{
	public static class StatisticsExtensions
	{
		public static void StatisticsMatch(this IList<IStatistics> stats, IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport)
		{
			foreach (IStatistics stat in stats)
			{
				stat.HandleMatch(homeTeam, awayTeam, matchReport);
			}
		}

		public static void StatisticsAction(this IList<IStatistics> stats, IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action)
		{
			foreach (IStatistics stat in stats)
			{
				stat.HandleAction(homeTeam, awayTeam, action);
			}
		}

		public static void StatisticsPrint(this IList<IStatistics> stats)
		{
			foreach (IStatistics stat in stats)
			{
				Console.WriteLine(stat.ToString());
			}
		}
	}
}