using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics
{
	public interface IStatistics
	{
		void HandleMatch(IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport);
		void HandleAction(IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action);
	}
}
