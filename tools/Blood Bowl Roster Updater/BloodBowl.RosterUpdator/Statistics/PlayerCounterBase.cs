using System.Collections.Generic;
using System.Linq;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;
using BloodBowl.MatchReport;

namespace BloodBowl.RosterUpdator.Statistics
{
	public abstract class PlayerCounterBase : IStatistics
	{
		private readonly IDictionary<string, int> _playerPoints = new Dictionary<string, int>();

		public abstract void HandleMatch(IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport);

		public abstract void HandleAction(IRoster homeTeam, IRoster awayTeam, ParsedMatchAction action);

		protected IEnumerable<KeyValuePair<string, int>> GetTop(int count)
		{
			 return _playerPoints.OrderBy(x => x.Value).Reverse().Take(count);
		}

		protected void AddPoints(IPlayer player, int points)
		{
			if (player.IsEmpty == true || player.IsJourneyPlayer == true)
			{
				return;
			}
			string key = player.ToString();
			if (_playerPoints.ContainsKey(key) == false)
			{
				_playerPoints[key] = 0;
			}
			_playerPoints[key] += points;
		}
	}
}
