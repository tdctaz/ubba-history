using System;
using System.ComponentModel;
using System.Globalization;

namespace BloodBowl.DomainModel
{
	public enum Injury
	{
		None = 0,

		BadlyHurt = 11,

		BrokenRibs = 41,
		GroinStrain = 42,
		GougedEye = 43,
		BrokenJaw = 44,
		FracturedArm = 45,
		FracturedLeg = 46,
		SmashedHand = 47,
		PinchedNerve = 48,

		DamagedBack = 51,
		SmashedKnee = 52,
		SmashedHip = 53,
		SmashedAnkle = 54,
		SeriousConcussion = 55,
		FracturedSkull = 56,
		BrokenNeck = 57,
		SmashedCollarBone = 58,

		Dead = 61,
	}

	public enum InjuryType
	{
		None,
		BadlyHurt,
		MissNextGame,
		NigglingInjury,
		ReducedMovement,
		ReducedArmor,
		ReducedAgility,
		ReducedStrength,
		Dead,
	}

	public static class InjuryExtensions
	{
		public static InjuryType GetInjuryType(this Injury injury)
		{
			switch (injury)
			{
			case Injury.BadlyHurt:
				return InjuryType.BadlyHurt;
			case Injury.BrokenRibs:
			case Injury.GroinStrain:
			case Injury.GougedEye:
			case Injury.BrokenJaw:
			case Injury.FracturedArm:
			case Injury.FracturedLeg:
			case Injury.SmashedHand:
			case Injury.PinchedNerve:
				return InjuryType.MissNextGame;
			case Injury.DamagedBack:
			case Injury.SmashedKnee:
				return InjuryType.NigglingInjury;
			case Injury.SmashedHip:
			case Injury.SmashedAnkle:
				return InjuryType.ReducedMovement;
			case Injury.SeriousConcussion:
			case Injury.FracturedSkull:
				return InjuryType.ReducedArmor;
			case Injury.BrokenNeck:
				return InjuryType.ReducedAgility;
			case Injury.SmashedCollarBone:
				return InjuryType.ReducedStrength;
			case Injury.Dead:
				return InjuryType.Dead;
			default:
				throw new NotImplementedException("Injury " + injury + " is not implemented");
			}
		}

		public static Injury GetFromString(string injuryString)
		{
			int injuryNumber;
			if (int.TryParse(injuryString, NumberStyles.Any, CultureInfo.InvariantCulture, out injuryNumber))
			{
				if (injuryNumber >= 11 && injuryNumber <= 38)
				{
					return Injury.BadlyHurt;
				}
				if (injuryNumber >= 61 && injuryNumber <= 68)
				{
					return Injury.Dead;
				}
				return (Injury)injuryNumber;
			}
			Injury injury;
			if (Enum.TryParse(injuryString.Replace(" ", ""), true, out injury))
			{
				return injury;
			}
			throw new InvalidEnumArgumentException("The injury " + injuryString + " is an unknown injury.");
		}
	}
}
