using System;
using System.Runtime.Serialization;

namespace BloodBowl.DomainModel.Exceptions
{
	public class BloodBowlFormatException : FormatException
	{
		public BloodBowlFormatException()
		{
		}

		public BloodBowlFormatException(string message)
				: base(message)
		{
		}

		public BloodBowlFormatException(string message, Exception innerException)
				: base(message, innerException)
		{
		}

		protected BloodBowlFormatException(SerializationInfo info, StreamingContext context)
				: base(info, context)
		{
		}
	}
}
