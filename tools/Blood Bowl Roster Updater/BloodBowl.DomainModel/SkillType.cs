using BloodBowl.DomainModel.Exceptions;

namespace BloodBowl.DomainModel
{
	public enum SkillType : byte
	{
		None = 0,

		// General 
		Block = 1,
		Dauntless = 2,
		DirtyPlayer = 3,
		Fend = 4,
		Frenzy = 5,
		Kick = 6,
		KickOffReturn = 7,
		PassBlock = 8,
		Pro = 9,
		Shadowing = 10,
		StripBall = 11,
		SureHands = 12,
		Tackle = 13,
		Wrestle = 14,

		// Agility
		Catch = 15,
		DivingCatch = 16,
		DivingTackle = 17,
		Dodge = 18,
		JumpUp = 19,
		Leap = 20,
		SideStep = 21,
		SneakyGit = 22,
		Sprint = 23,
		SureFeet = 24,

		// Passing
		Accurate = 25,
		DumpOff = 26,
		HailMaryPass = 27,
		Leader = 28,
		NervesOfSteel = 29,
		Pass = 30,
		SafeThrow = 31,

		// Strength
		BreakTackle = 32,
		Grab = 33,
		Guard = 34,
		Juggernaut = 35,
		MightyBlow = 36,
		MultipleBlock = 37,
		PilingOn = 38,
		StandFirm = 39,
		StrongArm = 40,
		ThickSkull = 41,

		// Mutations
		BigHand = 42,
		Claws = 43,
		DisturbingPresence = 44,
		ExtraArms = 45,
		FoulAppearance = 46,
		Horns = 47,
		PrehensileTail = 48,
		Tentacles = 49,
		TwoHeads = 50,
		VeryLongLegs = 51,

		// Extraordinary
		AlwaysHungry = 52,
		Animosity = 53,
		BallAndChain = 54,
		BloodLust = 55,
		Bombardier = 56,
		BoneHead = 57,
		Chainsaw = 58,
		Decay = 59,
		FanFavourite = 60,
		HypnoticGaze = 61,
		Loner = 62,
		NoHands = 63,
		NurglesRot = 64,
		ReallyStupid = 65,
		Regeneration = 66,
		RightStuff = 67,
		SecretWeapon = 68,
		Stab = 69,
		Stakes = 70,
		Stunty = 71,
		TakeRoot = 72,
		ThrowTeamMate = 73,
		Titchy = 74,
		WildAnimal = 75,

		// Stat Upgrades
		IncreasedMA = 100,
		IncreasedAG = 101,
		IncreasedST = 102,
		IncreasedAV = 103,
	}

	public static class StillTypeExtensions
	{
		public static SkillType Parse(string skillname)
		{
			switch(skillname)
			{
			case "block":
				return SkillType.Block;
			case "dauntless":
				return SkillType.Dauntless;
			case "dirtyplayer":
				return SkillType.DirtyPlayer;
			case "fend":
				return SkillType.Fend;
			case "frenzy":
				return SkillType.Frenzy;
			case "kick":
				return SkillType.Kick;
			case "kickoff":
			case "kickoffreturn":
				return SkillType.KickOffReturn;
			case "passblock":
				return SkillType.PassBlock;
			case "pro":
				return SkillType.Pro;
			case "shadowing":
				return SkillType.Shadowing;
			case "stripball":
				return SkillType.StripBall;
			case "surehands":
				return SkillType.SureHands;
			case "tackle":
				return SkillType.Tackle;
			case "wrestle":
				return SkillType.Wrestle;
			case "catch":
				return SkillType.Catch;
			case "divingcatch":
				return SkillType.DivingCatch;
			case "divingtackle":
				return SkillType.DivingTackle;
			case "dodge":
				return SkillType.Dodge;
			case "jumpup":
				return SkillType.JumpUp;
			case "leap":
				return SkillType.Leap;
			case "sidestep":
				return SkillType.SideStep;
			case "sneaky":
			case "sneakygit":
				return SkillType.SneakyGit;
			case "sprint":
				return SkillType.Sprint;
			case "surefeet":
				return SkillType.SureFeet;
			case "accurate":
				return SkillType.Accurate;
			case "dumpoff":
			case "dump-off":
				return SkillType.DumpOff;
			case "hmp":
			case "hailmary":
			case "hailmarypass":
				return SkillType.HailMaryPass;
			case "leader":
				return SkillType.Leader;
			case "nervesofsteel":
				return SkillType.NervesOfSteel;
			case "pass":
				return SkillType.Pass;
			case "safethrow":
				return SkillType.SafeThrow;
			case "breaktackle":
				return SkillType.BreakTackle;
			case "grab":
				return SkillType.Grab;
			case "guard":
				return SkillType.Guard;
			case "juggernaut":
				return SkillType.Juggernaut;
			case "mb":
			case "mightyblow":
				return SkillType.MightyBlow;
			case "multipleblock":
				return SkillType.MultipleBlock;
			case "po":
			case "pilingon":
			case "piling-on":
				return SkillType.PilingOn;
			case "standfirm":
				return SkillType.StandFirm;
			case "strongarm":
				return SkillType.StrongArm;
			case "ts":
			case "thickskull":
				return SkillType.ThickSkull;
			case "bighand":
				return SkillType.BigHand;
			case "claw/claws":
			case "claw":
			case "claws":
				return SkillType.Claws;
			case "disturbingpre":
			case "disturbingpresence":
				return SkillType.DisturbingPresence;
			case "extraarm":
			case "extraarms":
				return SkillType.ExtraArms;
			case "foulapp":
			case "foulappearance":
				return SkillType.FoulAppearance;
			case "horn":
			case "horns":
				return SkillType.Horns;
			case "prehensiletail":
				return SkillType.PrehensileTail;
			case "tentacles":
				return SkillType.Tentacles;
			case "twoheads":
				return SkillType.TwoHeads;
			case "verylonglegs":
				return SkillType.VeryLongLegs;
			case "ma":
			case "+ma":
				return SkillType.IncreasedMA;
			case "ag":
			case "+ag":
				return SkillType.IncreasedAG;
			case "st":
			case "+st":
				return SkillType.IncreasedST;
			case "av":
			case "+av":
				return SkillType.IncreasedAV;
			}
			throw new BloodBowlFormatException("Unknown skill type '" + skillname + "'");
		}

		public static string ToRosterString(this SkillType skillType)
		{
			switch (skillType)
			{
			case SkillType.DirtyPlayer:
				return "Dirty Player";
			case SkillType.KickOffReturn:
				return "Kick-Off Return";
			case SkillType.PassBlock:
				return "Pass Block";
			case SkillType.StripBall:
				return "Strip Ball";
			case SkillType.SureHands:
				return "Sure Hands";
			case SkillType.DivingCatch:
				return "Diving Catch";
			case SkillType.DivingTackle:
				return "Diving Tackle";
			case SkillType.JumpUp:
				return "Jump Up";
			case SkillType.SideStep:
				return "Side Step";
			case SkillType.SneakyGit:
				return "Sneaky Git";
			case SkillType.SureFeet:
				return "Sure Feet";
			case SkillType.DumpOff:
				return "Dump-Off";
			case SkillType.HailMaryPass:
				return "Hail Mary Pass";
			case SkillType.NervesOfSteel:
				return "Nerves Of Steel";
			case SkillType.SafeThrow:
				return "Safe Throw";
			case SkillType.BreakTackle:
				return "Break Tackle";
			case SkillType.MightyBlow:
				return "Mighty Blow";
			case SkillType.MultipleBlock:
				return "Multiple Block";
			case SkillType.PilingOn:
				return "Piling On";
			case SkillType.StandFirm:
				return "Stand Firm";
			case SkillType.StrongArm:
				return "Strong Arm";
			case SkillType.ThickSkull:
				return "Thick Skull";
			case SkillType.BigHand:
				return "Big Hand";
			case SkillType.Claws:
				return "Claw / Claws";
			case SkillType.DisturbingPresence:
				return "Disturbing Presence";
			case SkillType.ExtraArms:
				return "Extra Arms";
			case SkillType.FoulAppearance:
				return "Foul Appearance";
			case SkillType.PrehensileTail:
				return "Prehensile Tail";
			case SkillType.TwoHeads:
				return "Two Heads";
			case SkillType.VeryLongLegs:
				return "Very Long Legs";
			case SkillType.IncreasedMA:
				return "+MA";
			case SkillType.IncreasedAG:
				return "+AG";
			case SkillType.IncreasedST:
				return "+ST";
			case SkillType.IncreasedAV:
				return "+AV";
			default:
				return skillType.ToString();
			}
		}
	}
}
