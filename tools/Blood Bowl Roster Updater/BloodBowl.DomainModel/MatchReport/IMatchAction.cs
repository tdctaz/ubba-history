﻿namespace BloodBowl.DomainModel.MatchReport
{
	public interface IMatchAction
	{
		string Half { get; }
		int Turn { get; }
		string TeamTag { get; }
		int SourcePlayer { get; }
		string Action { get; }
		int TargetPlayer { get; }

		bool IsEmpty();
	}
}
