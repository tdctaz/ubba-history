using System.Collections.Generic;

namespace BloodBowl.DomainModel.MatchReport
{
	public interface IMatchReport
	{
		string MatchType { get; }
		string MatchNumber { get; }
		string Report { get; }
		ITeamReport HomeTeam { get; }
		ITeamReport AwayTeam { get;  }
		IEnumerable<IMatchAction> MatchActions { get; }
	}
}
