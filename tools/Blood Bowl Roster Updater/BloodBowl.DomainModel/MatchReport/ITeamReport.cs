﻿namespace BloodBowl.DomainModel.MatchReport
{
	public interface ITeamReport
	{
		string TeamName { get; }
		string TeamTag { get; }
		int Gate { get; }
		int FAME { get; }
		int PettyCash { get; }
		int Inducement { get; }
		int MVP { get; }
		int Winnings { get; }
		int FansRoll { get; }
		string[] Inducements { get; }
		int Score { get; }
	}
}
