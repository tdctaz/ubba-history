namespace BloodBowl.DomainModel.Roster
{
	public interface IMatchHistory
	{
		string OpponentTeamName { get; }
		string SeasonType { get; }
		int SeasonNumber { get; }
		int MatchNumber { get; }
		string Notes { get; }

		IMatchTeamHistory Self { get; }
		IMatchTeamHistory Opponent { get; }

		IMatchInjuries InjuriesToSelf { get; }
		IMatchInjuries InjuriesToOpponentFromTeam { get; }
		IMatchInjuries InjuriesToOpponentFromFans { get; }
		IMatchInjuries InjuriesFromOpponentsTeam { get; }
		IMatchInjuries InjuriesFromOpponentsFans { get; }
	}
}
