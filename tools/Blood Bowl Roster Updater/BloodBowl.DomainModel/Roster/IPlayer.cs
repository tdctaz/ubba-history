﻿namespace BloodBowl.DomainModel.Roster
{
	public interface IPlayer
	{
		int Number { get; }
		string Name { get; }
		string Type { get; }
		bool MissNextMatch { get; }
		int NigglingInjury { get; }
		int InjuriesMovement { get; }
		int InjuriesStrength { get; }
		int InjuriesAgility { get; }
		int InjuriesArmor { get; }
		int ActInterceptions { get; }
		int ActCompletions { get; }
		int ActTouchdowns { get; }
		int ActCasaulties { get; }
		int ActKills { get; }
		int MVPs { get; }
		string Upgrade1{ get; }
		string Upgrade2 { get; }
		string Upgrade3 { get; }
		string Upgrade4 { get; }
		string Upgrade5 { get; }
		string Upgrade6 { get; }
		string BonusUpgrades { get; }
		int BonusSSP { get; }
		int BonusValue { get; }

		bool IsEmpty { get; }
		bool IsJourneyPlayer { get; }
	}
}
