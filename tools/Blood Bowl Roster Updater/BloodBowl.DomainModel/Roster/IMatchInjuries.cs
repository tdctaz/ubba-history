﻿namespace BloodBowl.DomainModel.Roster
{
	public interface IMatchInjuries
	{
		int BadlyHurt { get; }
		int SeriousInjuries { get; }
		int Kills { get; }
	}
}
