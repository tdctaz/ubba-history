namespace BloodBowl.DomainModel.Roster
{
	public interface IPlayerDetails
	{
		string TypeName { get; }
		string ShortTypeName { get; }
		int Cost { get; }
		bool Journeyman { get; }
	}
}
