using System.Collections.Generic;

namespace BloodBowl.DomainModel.Roster
{
	public interface IRoster
	{
		string TeamName { get; }
		string TeamTag { get; }
		string Race { get; }
		string Coach { get; }
		int Treasury { get; }
		string NotesLeft { get; }
		string NotesRight { get; }
		int ReRolls { get; }
		int FanFactor { get; }
		int AssCoaches { get; }
		int Cheerleaders { get; }
		int Apothecary { get; }
		IPlayer[] Players { get; }
		IEnumerable<IMatchHistory> MatchHistory { get; }
		ITeamDetails Details { get; }
	}
}
