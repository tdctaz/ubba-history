﻿namespace BloodBowl.DomainModel.Roster
{
	public interface IMatchTeamHistory
	{
		int Touchdowns { get; }
		int Gate { get; }
		int Winnings { get; }
		int FanFactor { get; }
	}
}
