using System.Collections.Generic;

namespace BloodBowl.DomainModel.Roster
{
	public interface ITeamDetails
	{
		int ReRollCost { get; }
		IEnumerable<IPlayerDetails> PlayerDetails { get; }
	}
}
