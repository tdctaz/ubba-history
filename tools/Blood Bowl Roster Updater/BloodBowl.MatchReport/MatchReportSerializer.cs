using System.IO;
using System.Linq;
using BloodBowl.Core;
using BloodBowl.DomainModel.MatchReport;
using Microsoft.Office.Interop.Excel;

namespace BloodBowl.MatchReport
{
	public class MatchReportSerializer : ExcelSerializerBase
	{
		public IMatchReport ReadMatchReport(string filepath)
		{
			if (File.Exists(filepath) == false)
			{
				throw new FileNotFoundException("The match report was not found.", filepath);
			}

			var workbook = Workbooks.Open(filepath);
			try
			{
				Worksheet sheet = workbook.Sheets[1];
				var report = new MatchReport
				{
					HomeTeam = new TeamReport
					{
						TeamName = GetAsString(sheet, "C3"),
						TeamTag = GetAsString(sheet, "C3"),
						Gate = GetAsInt(sheet, "C4"),
						PettyCash = GetAsInt(sheet, "D5"),
						Inducement = GetAsInt(sheet, "D6"),
						Inducements = GetAsMultilinedString(sheet, "B7"),
						MVP = GetAsInt(sheet, "L25"),
						Winnings = GetAsInt(sheet, "M26"),
						FansRoll = GetAsInt(sheet, "N27"),
						Score = GetAsInt(sheet, "L28"),
						FAME = GetAsInt(sheet, "E4"),
					},
					AwayTeam = new TeamReport
					{
						TeamName = GetAsString(sheet, "G3"),
						TeamTag = GetAsString(sheet, "G3"),
						Gate = GetAsInt(sheet, "G4"),
						PettyCash = GetAsInt(sheet, "H5"),
						Inducement = GetAsInt(sheet, "H6"),
						Inducements = GetAsMultilinedString(sheet, "F7"),
						MVP = GetAsInt(sheet, "P25"),
						Winnings = GetAsInt(sheet, "Q26"),
						FansRoll = GetAsInt(sheet, "R27"),
						Score = GetAsInt(sheet, "P28"),
						FAME = GetAsInt(sheet, "I4"),
					},
					MatchSeason = GetAsInt(sheet, "C2"),
					MatchNumber = GetAsString(sheet, "H2"),
					MatchType = GetAsString(sheet, "E2"),
					Report = GetAsString(sheet, "B12"),
					MatchActions = 3.RangeTo(23).Select(x => GetMatchActions(sheet, x, 'K', 3))
						.Concat(3.RangeTo(28).Select(x => GetMatchActions(sheet, x, 'T', 1)))
						.ToArray(),
				};
				return report;
			}
			finally
			{
				workbook.Close(SaveChanges: false);
				ReleaseComObject(workbook);
			}
		}

		private MatchAction GetMatchActions(Worksheet sheet, int row, char column, int lastOffset)
		{
			var matchAction = new MatchAction
			{
				Half = GetAsString(sheet, column.ToString()+ row),
				Turn = GetAsInt(sheet, ((char)(column + 1)).ToString() + row),
				TeamTag = GetAsString(sheet, ((char)(column + (char)2)).ToString() + row),
				SourcePlayer = GetAsInt(sheet, ((char)(column + (char)3)).ToString() + row),
				Action = GetAsString(sheet, ((char)(column + (char)4)).ToString() + row),
				TargetPlayer = GetAsInt(sheet, ((char)(column + 4 + lastOffset)).ToString() + row),
			};
			return matchAction;
		}
	}
}
