using System;
using BloodBowl.DomainModel.MatchReport;

namespace BloodBowl.MatchReport
{
	public class MatchAction : IMatchAction, IEquatable<MatchAction>
	{
		public MatchAction()
		{
			Half = string.Empty;
			TeamTag = string.Empty;
			Action = string.Empty;
		}

		public string Half
		{
			get;
			set;
		}

		public int Turn
		{
			get;
			set;
		}

		public string TeamTag
		{
			get;
			set;
		}

		public int SourcePlayer
		{
			get;
			set;
		}

		public string Action
		{
			get;
			set;
		}

		public int TargetPlayer
		{
			get;
			set;
		}

		public bool IsEmpty()
		{
			return Equals(Empty);
		}

		private static readonly MatchAction Empty = new MatchAction();

		public bool Equals(MatchAction other)
		{
			if (ReferenceEquals(null, other))
			{
				return false;
			}
			if (ReferenceEquals(this, other))
			{
				return true;
			}
			return Equals(other.Half, Half) && other.Turn == Turn && Equals(other.TeamTag, TeamTag) && other.SourcePlayer == SourcePlayer && Equals(other.Action, Action) && other.TargetPlayer == TargetPlayer;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}
			if (ReferenceEquals(this, obj))
			{
				return true;
			}
			if (obj.GetType() != typeof(MatchAction))
			{
				return false;
			}
			return Equals((MatchAction)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int result = (Half != null ? Half.GetHashCode() : 0);
				result = (result * 397) ^ Turn;
				result = (result * 397) ^ (TeamTag != null ? TeamTag.GetHashCode() : 0);
				result = (result * 397) ^ SourcePlayer;
				result = (result * 397) ^ (Action != null ? Action.GetHashCode() : 0);
				result = (result * 397) ^ TargetPlayer;
				return result;
			}
		}
	}
}
