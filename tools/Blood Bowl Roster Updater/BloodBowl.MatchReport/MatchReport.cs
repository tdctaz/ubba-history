using System.Collections.Generic;
using BloodBowl.DomainModel.MatchReport;

namespace BloodBowl.MatchReport
{
	public class MatchReport : IMatchReport
	{
		public int MatchSeason
		{
			get;
			set;
		}

		public string MatchType
		{
			get;
			set;
		}

		public string MatchNumber
		{
			get;
			set;
		}

		public string Report
		{
			get;
			set;
		}

		public ITeamReport HomeTeam
		{
			get;
			set;
		}

		public ITeamReport AwayTeam
		{
			get;
			set;
		}

		public IEnumerable<IMatchAction> MatchActions
		{
			get;
			set;
		}
	}
}
