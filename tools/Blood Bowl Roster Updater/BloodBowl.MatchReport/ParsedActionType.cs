﻿namespace BloodBowl.MatchReport
{
	public enum ParsedActionType
	{
		None, // no action performed

		Pass,
		Interception,
		Touchdown,
		Casualty,
		PushOut,
		Foul,
		FailedDodge,
		FailedGfi,
		LevelUp,
		Hire,
		Fire,
		Purchase,
		Wizard,
		Eat,
		Stab,
		Bomb,
		ThrowRock,
		Decay,
		Reanimate,
		HireJourneyman,

		Conceded, // 2-0 loss
	}
}
