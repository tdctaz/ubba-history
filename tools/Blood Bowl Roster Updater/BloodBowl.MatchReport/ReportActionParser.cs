using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;

namespace BloodBowl.MatchReport
{
	public static class ParsedMatchActionExtensions
	{
		public static ParsedMatchAction Parse(this IMatchAction matchAction, IRoster homeRoster, IRoster awayRoster, IMatchReport matchReport)
		{
			return new ParsedMatchAction(matchAction, homeRoster, awayRoster, matchReport);
		}
	}
}
