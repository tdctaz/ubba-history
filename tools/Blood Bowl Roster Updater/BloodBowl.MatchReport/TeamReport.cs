using BloodBowl.DomainModel.MatchReport;

namespace BloodBowl.MatchReport
{
	public class TeamReport : ITeamReport
	{
		public string TeamName
		{
			get;
			set;
		}

		public string TeamTag
		{
			get;
			set;
		}

		public int Gate
		{
			get;
			set;
		}

		public int FAME
		{
			get;
			set;
		}

		public int PettyCash
		{
			get;
			set;
		}

		public int Inducement
		{
			get;
			set;
		}

		public string[] Inducements
		{
			get;
			set;
		}

		public int MVP
		{
			get;
			set;
		}

		public int Winnings
		{
			get;
			set;
		}

		public int FansRoll
		{
			get;
			set;
		}

		public int Score
		{
			get;
			set;
		}
	}
}
