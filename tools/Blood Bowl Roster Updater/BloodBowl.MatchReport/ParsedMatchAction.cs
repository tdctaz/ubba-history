using System;
using System.Linq;
using BloodBowl.Core;
using BloodBowl.DomainModel;
using BloodBowl.DomainModel.Exceptions;
using BloodBowl.DomainModel.MatchReport;
using BloodBowl.DomainModel.Roster;

namespace BloodBowl.MatchReport
{
	public class ParsedMatchAction
	{
		private readonly IMatchAction _matchAction;

		private ParsedMatchAction()
		{
			Item = ItemType.None;
			Type = ParsedActionType.None;
			Skill = SkillType.None;
			Name = string.Empty;
			Injury = Injury.None;
			Regenerated = false;
			ApothecaryInjury = Injury.None;
		}

		public ParsedMatchAction(IMatchAction matchAction, IRoster homeTeam, IRoster awayTeam, IMatchReport matchReport)
			: this()
		{
			_matchAction = matchAction;
			if (matchAction.IsEmpty() == false)
			{
				SourceTeam = GetActionTeam(matchAction, homeTeam, awayTeam);
				SourcePlayer = _matchAction.SourcePlayer == 0 ? null : GetPlayer(SourceTeam, _matchAction.SourcePlayer);
				TargetTeam = SourceTeam == homeTeam ? awayTeam : homeTeam;
				TargetPlayer = _matchAction.TargetPlayer == 0 ? null : GetPlayer(TargetTeam, _matchAction.TargetPlayer);
				Half = GetHalf(_matchAction.Half);
				Turn = _matchAction.Turn;
				Parse(GetActionTokens(), GetTokens(), matchReport);
			}
		}

		public string ActionText
		{
			get
			{
				return _matchAction.Action;
			}
		}

		public ParsedActionType Type
		{
			get; private set;
		}

		public SkillType Skill
		{
			get; private set;
		}

		public Injury Injury
		{
			get; private set;
		}

		public bool Regenerated
		{
			get; private set;
		}

		public Injury ApothecaryInjury
		{
			get; private set;
		}

		public IPlayer SourcePlayer
		{
			get; private set;
		}

		public IRoster SourceTeam
		{
			get; private set;
		}

		public IPlayer TargetPlayer
		{
			get; private set;
		}

		public IRoster TargetTeam
		{
			get; private set;
		}

		public int Half
		{
			get; private set;
		}

		public int Turn
		{
			get; private set;
		}

		public int Price
		{
			get; private set;
		}

		public string Name
		{
			get; private set;
		}

		public string PlayerType
		{
			get; private set;
		}

		public ItemType Item
		{
			get; private set;
		}

		private string[] GetActionTokens()
		{
			string[] actionTokens = GetTokens()
				.Select(x => x.ToStrippedLowerCase())
				.Where(x => string.IsNullOrEmpty(x) == false)
				.ToArray();
			return actionTokens;
		}

		private string[] GetTokens()
		{
			string[] tokens = _matchAction.Action
				.Split(new[] { "(", ")", ",", "->" }, StringSplitOptions.None)
				.Select(x => x.Trim())
				.Where(x => string.IsNullOrEmpty(x) == false)
				.ToArray();
			return tokens;
		}

		private void Parse(string[] actionTokens, string[] originalTokens, IMatchReport matchReport)
		{
			string actionTypeFormated = actionTokens[0];
			if (actionTypeFormated == "cas" || actionTypeFormated == "casaulty" || actionTypeFormated == "kill" || actionTypeFormated == "kills")
			{
				ParseCasaulty(actionTokens);
			}
			else if (actionTypeFormated == "faileddodge" || actionTypeFormated == "fd")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.FailedDodge);
			}
			else if (actionTypeFormated == "failedgoingforit" || actionTypeFormated == "failedgfi" || actionTypeFormated == "fgfi")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.FailedGfi);
			}
			else if (actionTypeFormated == "foul")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.Foul);
			}
			else if (actionTypeFormated == "eaten" || actionTypeFormated == "eats" || actionTypeFormated == "eat")
			{
				ParseInjuryTeammateAction(actionTokens, ParsedActionType.Eat);
			}
			else if (actionTypeFormated == "decay")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.Decay);
			}
			else if (actionTypeFormated == "stabs" || actionTypeFormated == "stab")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.Stab);
			}
			else if (actionTypeFormated == "bomb" || actionTypeFormated == "bombs" || actionTypeFormated == "bombopponent")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.Bomb);
			}
			else if (actionTypeFormated == "throwrock" || actionTypeFormated == "rock")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.ThrowRock);
			}
			else if (actionTypeFormated == "bombteammate" || actionTypeFormated == "bombsteammate")
			{
				ParseInjuryTeammateAction(actionTokens, ParsedActionType.Bomb);
			}
			else if (actionTypeFormated == "hire")
			{
				ParseHire(actionTokens, originalTokens);
			}
			else if (actionTypeFormated == "hirejourneyman")
			{
				ParseHireJourneyman(actionTokens, originalTokens);
			}
			else if (actionTypeFormated == "reanimate")
			{
				ParseHire(actionTokens, originalTokens, free: true);
			}
			else if (actionTypeFormated == "int" || actionTypeFormated == "interception")
			{
				Type = ParsedActionType.Interception;
			}
			else if (actionTypeFormated == "levelup" || actionTypeFormated == "level-up")
			{
				ParseLevelUp(actionTokens);
			}
			else if (actionTypeFormated == "pass" || actionTypeFormated == "comp" || actionTypeFormated == "completion")
			{
				Type = ParsedActionType.Pass;
				TargetTeam = SourceTeam;
				TargetPlayer = _matchAction.TargetPlayer == 0 ? null : GetPlayer(SourceTeam, _matchAction.TargetPlayer);
			}
			else if (actionTypeFormated == "purchage" || actionTypeFormated == "buy" || actionTypeFormated == "buys")
			{
				ParsePurchage(actionTokens);
			}
			else if (actionTypeFormated == "pushout")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.PushOut);
			}
			else if (actionTypeFormated == "pushoutteammate")
			{
				ParseInjuryTeammateAction(actionTokens, ParsedActionType.PushOut);
			}
			else if (actionTypeFormated == "sack" || actionTypeFormated == "fire")
			{
				Type = ParsedActionType.Fire;
			}
			else if (actionTypeFormated == "touchdown" || actionTypeFormated == "td")
			{
				Type = ParsedActionType.Touchdown;
			}
			else if (actionTypeFormated == "wizard" || actionTypeFormated == "fireball" || actionTypeFormated == "lighting" || actionTypeFormated == "lightningball")
			{
				ParseInjuryAction(actionTokens, ParsedActionType.Wizard);
			}
			else if(actionTypeFormated == "wizardteammate" || actionTypeFormated == "fireballteammate")
			{
				ParseInjuryTeammateAction(actionTokens, ParsedActionType.Wizard);
			}
			else if(actionTypeFormated == "conceded")
			{
				var matchReportTeam = matchReport.AwayTeam.TeamTag == SourceTeam.TeamTag ? matchReport.AwayTeam : matchReport.HomeTeam;
				Type = ParsedActionType.Conceded;
				SourcePlayer = SourceTeam.Players[matchReportTeam.MVP - 1];
				Price = matchReportTeam.Winnings;
			}
			else
			{
				throw new BloodBowlFormatException("The action type '" + actionTokens[0] + "'");
			}
		}

		private void ParseInjuryTeammateAction(string[] actionTokens, ParsedActionType resultActionType)
		{
			ParseInjuryAction(actionTokens, resultActionType);
			TargetTeam = SourceTeam;
			TargetPlayer = _matchAction.TargetPlayer == 0 ? null : GetPlayer(SourceTeam, _matchAction.TargetPlayer);
		}

		private void ParseInjuryAction(string[] actionTokens, ParsedActionType actionType)
		{
			Type = actionType;
			if (actionTokens.Length >= 2)
			{
				ParseInjuries(actionTokens, 1);
			}
		}

		private void ParsePurchage(string[] actionTokens)
		{
			Type = ParsedActionType.Purchase;
			string itemName = actionTokens[1];
			if (itemName == "apothecary" || itemName == "apo")
			{
				Item = ItemType.Apothecary;
				Price = 50;
			}
			else if (itemName == "reroll" || itemName == "rr")
			{
				Item = ItemType.ReRoll;
				Price = SourceTeam.Details.ReRollCost * 2;
			}
			else if (itemName == "cheerleader")
			{
				Item = ItemType.Cheerleader;
				Price = 10;
			}
			else if (itemName == "asscoach" || itemName == "coach")
			{
				Item = ItemType.AssCoaches;
				Price = 10;
			}
			else
			{
				throw new BloodBowlFormatException("Unknown item '" + itemName + "' purchaged");
			}
		}

		private void ParseLevelUp(string[] actionTokens)
		{
			Type = ParsedActionType.LevelUp;
			// TODO: determine skill roll
			Skill = StillTypeExtensions.Parse(actionTokens[2]);
		}

		private void ParseHire(string[] actionTokens, string[] originalTokens, bool free = false)
		{
			var playerDetails = SourceTeam.Details.PlayerDetails
				.Single(x => x.ShortTypeName.ToStrippedLowerCase() == actionTokens[2]);
			Name = originalTokens[1];
			PlayerType = playerDetails.TypeName;
			if (free)
			{
				Price = 0;
				Type = ParsedActionType.Reanimate;
			}
			else
			{
				Price = playerDetails.Cost;
				Type = ParsedActionType.Hire;
			}
		}

		public void ParseHireJourneyman(string[] actionTokens, string[] originalTokens)
		{
			var playerDetails = SourceTeam.Details.PlayerDetails
				.Single(x => x.ShortTypeName.ToStrippedLowerCase() == actionTokens[2]);
			Name = originalTokens[1];
			PlayerType = playerDetails.TypeName;
			Price = playerDetails.Cost;
			Type = ParsedActionType.HireJourneyman;
		}

		private void ParseCasaulty(string[] actionTokens)
		{
			Type = ParsedActionType.Casualty;
			if (actionTokens[0] == "kill" || actionTokens[0] == "kills")
			{
				Injury = Injury.Dead;
			}
			else if (actionTokens.Length >= 2)
			{
				ParseInjuries(actionTokens, 1);
			}
			else
			{
				throw new BloodBowlFormatException("The casaulty action string '" + _matchAction.Action + "' does not contain any injury type");
			}
		}

		private void ParseInjuries(string[] actionTokens, int offset)
		{
			Injury = InjuryExtensions.GetFromString(actionTokens[offset]);
			if (actionTokens.Length > 1 + offset)
			{
				if (IsRegenerated(actionTokens[1 + offset]))
				{
					Regenerated = true;
				}
				else
				{
					ApothecaryInjury = InjuryExtensions.GetFromString(actionTokens[2]);
				}
			}
			if (actionTokens.Length > 2 + offset && IsRegenerated(actionTokens[2 + offset]))
			{
				Regenerated = true;
			}
		}

		private static bool IsRegenerated(string token)
		{
			return token == "regenerated" || token == "regen";
		}

		private static int GetHalf(string half)
		{
			if (string.IsNullOrEmpty(half))
			{
				return 0;
			}
			switch (half)
			{
			case "1st":
			case "1":
			case "first":
				return 1;
			case "2nd":
			case "2":
			case "second":
				return 2;
			case "overtime":
				return 3;
			}
			throw new FormatException("Unknown half '" + half + "'");
		}

		private static IPlayer GetPlayer(IRoster roster, int playerNumber)
		{
			if (playerNumber < 1 || playerNumber > 16)
			{
				throw new IndexOutOfRangeException("Unknown player number " + playerNumber);
			}
			return roster.Players[playerNumber - 1];
		}

		private static IRoster GetActionTeam(IMatchAction matchAction, params IRoster[] teams)
		{
			return teams.Single(x => x.TeamTag.Equals(matchAction.TeamTag, StringComparison.InvariantCultureIgnoreCase));
		}

		public override string ToString()
		{
			string sourcePlayer = GetPlayerName(SourcePlayer, SourceTeam);
			string targetPlayer = GetPlayerName(TargetPlayer, TargetTeam);

			return string.Format("{0}.{1} - {2}: {3} {4}", Half, Turn, sourcePlayer, _matchAction.Action, targetPlayer);
		}

		private static string GetPlayerName(IPlayer player, IRoster team)
		{
			if (player != null)
			{
				if (player.IsEmpty)
				{
					return string.Format("[#{0}({1}) <temporary>]", player.Number, team.TeamTag);
				}
				return string.Format("[#{0}({1}) {2}]", player.Number, team.TeamTag, player.Name);
			}
			return string.Empty;
		}
	}
}
