using System;
using System.IO;
using System.Linq;
using Xunit;
using Xunit.Extensions;

namespace BloodBowl.Roster.UnitTest
{
	public class RosterSerializerTest
	{
		private readonly RosterSerializer _rosterSerializer;

		public RosterSerializerTest()
		{
			_rosterSerializer = new RosterSerializer();
		}

		[Fact]
		public void GivenNonExistingFile_WhenReadingRosterFile_ThenFileNotFoundExceptionIsThrown()
		{
			string filepath = Path.Combine(TestFilesPath, "nonexistingfile.xlsx");

			Assert.ThrowsDelegateWithReturn act = () => _rosterSerializer.ReadRoster(filepath);

			Assert.Throws<FileNotFoundException>(act);
		}

		[Fact]
		public void GivenEmptyRosterFile_WhenReadingRosterFile_ThenDefaultRosterIsOpened()
		{
			string filepath = Path.Combine(TestFilesPath, "emptyroster.xlsx");

			var roster = _rosterSerializer.ReadRoster(filepath);

			Assert.Equal(0, roster.Apothecary);
			Assert.Equal(0, roster.AssCoaches);
			Assert.Equal(0, roster.Cheerleaders);
			Assert.Equal(string.Empty, roster.Coach);
			Assert.Equal(5, roster.FanFactor);
			Assert.Equal(string.Empty, roster.NotesLeft);
			Assert.Equal("Amazon", roster.Race);
			Assert.Equal(0, roster.ReRolls);
			Assert.Equal(string.Empty, roster.TeamName);
			Assert.Equal(string.Empty, roster.TeamTag);
			Assert.Equal(16, roster.Players.Length);
		}


		[Fact]
		public void GivenEmptyRosterFile_WhenReadingRosterFile_ThenRosterReRollPriceIsRead()
		{
			string filepath = Path.Combine(TestFilesPath, "emptyroster.xlsx");

			var roster = _rosterSerializer.ReadRoster(filepath);

			Assert.Equal(50, roster.Details.ReRollCost);
		}

		[InlineData("Amazon Linewoman", 50)]
		[InlineData("Amazon Thrower", 70)]
		[InlineData("Amazon Catcher", 70)]
		[InlineData("Amazon Blitzer", 90)]
		[InlineData("Amazon Journeywoman", 50)]
		[Theory]
		public void GivenEmptyRosterFile_WhenReadingRosterFile_ThenRosterNamesAndCostIsRead(string name, int cost)
		{
			string filepath = Path.Combine(TestFilesPath, "emptyroster.xlsx");

			var roster = _rosterSerializer.ReadRoster(filepath);

			Assert.Equal(cost, roster.Details.PlayerDetails.Where(x => x.TypeName == name).Single().Cost);
		}

		private static string TestFilesPath
		{
			get
			{
				return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfiles");
			}
		}
	}
}
